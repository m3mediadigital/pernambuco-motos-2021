<?php

use App\Models\Showroom;
use App\Models\Setting;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('honda-dream', function () {
    return view('dreams',[
        'dreams' => Showroom::customDream(),
        'settings' => Setting::customFetchAll()
    ]);
});

Route::get('{page?}/{slug?}', function () {
    return view('index');
});
