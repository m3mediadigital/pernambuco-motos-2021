import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import Home from "../home/Home";

import Acessorios from "../components/internas/Acessorios";
import AnaliseCredito from "../components/internas/AnaliseCredito";
import Contato from "../components/internas/Contato";
import Consorcio from "../components/internas/Consorcio";
import MonteSeu from "../components/internas/MonteSeu";
import Oferta from "../components/internas/Oferta";
import OfertasLista from "../components/internas/OfertasLista";
import QuemSomos from "../components/internas/QuemSomos";
import Seminovo from "../components/internas/Seminovo";
import SeminovoLista from "../components/internas/SeminovosLista";
import PosVendaLista from "../components/internas/PosVendaLista";
import PosVenda from "../components/internas/PosVenda";
import Showroom from "../components/internas/Showroom";
import ShowroomLista from "../components/internas/ShowroomLista";
import TesteDrive from "../components/internas/TesteDrive";
import VendasDiretasLista from "../components/internas/VendaDiretaLista";
import VendasDireta from "../components/internas/VendaDireta";
// import Empresa from "../components/internas/Empresas";


export default (props) => (
    <BrowserRouter>
        <Switch>
            <Route path="/acessorios" component={Acessorios} />
            <Route path="/acessorio/:slug" component={Acessorios} />
            <Route path="/analise-credito" component={AnaliseCredito} />
            <Route path="/contato" component={Contato} />
            <Route path="/consorcio" component={Consorcio} />
            <Route path="/monte-o-seu" component={MonteSeu} />
            <Route path="/oferta/:slug" component={Oferta} />
            <Route path="/ofertas" component={OfertasLista} />
            <Route path="/quem-somos" component={QuemSomos} />
            <Route path="/seminovo/:slug" component={Seminovo} />
            <Route path="/seminovos" component={SeminovoLista} />
            <Route path="/servicos" component={PosVendaLista} />
            <Route path="/pos-venda/:slug" component={PosVenda} />
            <Route path="/showroom/:slug" component={Showroom} />
            <Route path="/showrooms" component={ShowroomLista} />
            <Route path="/test-drive" component={TesteDrive} />
            <Route path="/venda-direta/:slug" component={VendasDireta} />
            <Route path="/vendas-diretas" component={VendasDiretasLista} />
            {/* <Route path="/empresas" component={Empresa} /> */}

            <Route exact path="/" component={Home} />
        </Switch>
    </BrowserRouter>
);
