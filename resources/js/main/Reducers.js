import { combineReducers } from 'redux'

import AcessoriosReducer from '../reducers/AcessoriosReducer'
import ContatosReducer from '../reducers/ContatosReducer'
import InstitucionalReducer from '../reducers/InstitucionalReducer'
import NovasReducer from '../reducers/NovasReducer'
import ConsorciosReducer from '../reducers/ConsorciosReducer'
import OfertasReducer from '../reducers/OfertasReducer'
import SlidesReducer from '../reducers/SlidesReducer'
import SettingsReducer from '../reducers/SettingsReducer'
import SeminovosReducer from '../reducers/SeminovosReducer'
import VendasDiretasReducer from '../reducers/VendasDiretasReducer'

const rootReducer = combineReducers({
  acessorios: AcessoriosReducer,
  contatos: ContatosReducer,
  institucional: InstitucionalReducer,
  novas: NovasReducer,
  ofertas: OfertasReducer,
  seminovos: SeminovosReducer,
  settings: SettingsReducer,
  slides: SlidesReducer,
  vendas_diretas: VendasDiretasReducer,
  consorcios: ConsorciosReducer
})

export default rootReducer