import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";
import banner_contact from "../../assets/images/banner-contato.png";

import {sendContato} from "../../actions/ContatosAction";

import InputMask from "react-input-mask";
import ScriptsSend from "../helpers/ScriptsSend";

class Contato extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nome: "",
            email: "",
            pcd: "",
            telefone: "",
            mensagem: "",
            meio_contato: "",
            sending: false,
        };
        this.method = "send-contato";
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    clearForm(field) {
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state[field] = field === "sending" ? false : "";
        return this.state[field];
    }
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    }
    hasErrorFor(field) {
        return !!this.props.error_contato.get(field);
    }
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state["sending"] = false;
            return (
                <span className="error">
                    <strong>{this.props.error_contato.get(field).get(0)}</strong>
                </span>
            );
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
        });

        const formData = new FormData();
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", "contato");
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("pcd", this.state.pcd);
        formData.append("mensagem", this.state.mensagem);
        formData.append("assunto", "Formulario de Contato");
        formData.append("unidade", "Matriz");
        formData.append("meio_contato", this.state.meio_contato);

        this.props.sendContato(this.method, formData);
    }

    render() {
        const {success_contato} = this.props;

        var divStyle = {
            maxWidth: "none",
            width: "auto",
        };
        return (
            <Main>
                <div className="container-fluid contato" style={{backgroundImage: "linear-gradient(#ffffffa8, #ffffffa8),url(" + banner_contact + ")", backgroundSize: "cover"}}>
                {/* <div className="container-fluid contato"> */}
                    <div className="row">
                        <div className="col-12 col-lg-6">
                            <div className="text-side" style={divStyle}>
                                <TitleSection title="Ta com dúvida" subtitle="Fale conosco" />
                                <p>
                                    Nós queremos ouvir as suas dúvidas e sugestões. <br />
                                    Fique à vontade para nos enviar uma mensagem e em breve entraremos em contato com você.
                                </p>
                            </div>
                        </div>
                        <div className="col-12 col-lg-6">
                            <div className="form-side border">
                                <form className="container" onSubmit={this.handleSubmit} method="post">
                                    <div className="row">
                                        <div className="form-group col-12 ">
                                            <input type="text" placeholder="Nome e Sobrenome" className={`form-control ${this.hasErrorFor("nome") ? "error" : ""}`} name="nome" value={success_contato.size > 0 ? this.clearForm("nome") : this.state.nome} onChange={this.handleChange} />
                                            {this.renderErrorFor("nome")}
                                        </div>
                                        <div className="form-group col-12">
                                            <InputMask mask="(99) 99999-9999" type="tel" name="telefone" className={`form-control ${this.hasErrorFor("telefone") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("telefone") : this.state.telefone} placeholder="Telefone" onChange={this.handleChange} />
                                            {this.renderErrorFor("telefone")}
                                        </div>
                                        {/* <div className="form-group col-12 col-lg-4">
                                            <select name="pcd" id="pcd" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("pcd") ? "error" : ""}`}>
                                                <option value="">PCD?</option>
                                                <option value="Sim">Sim</option>
                                                <option value="Nao">Nao</option>
                                            </select>
                                            {this.renderErrorFor("pcd")}
                                        </div> */}
                                        <div className="form-group col-12">
                                            <input type="email" name="email" placeholder="Email" className={`form-control ${this.hasErrorFor("email") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("email") : this.state.email} onChange={this.handleChange} />
                                            {this.renderErrorFor("email")}
                                        </div>
                                        <div className="form-group col-12">
                                            <select name="meio_contato" id="meio_contato" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("meio_contato") ? "error" : ""}`}>
                                                <option value="">Por onde deseja ser contatado?</option>
                                                <option value="Email">Email</option>
                                                <option value="Telefone">Telefone</option>
                                                <option value="Whatsapp">Whatsapp</option>
                                            </select>
                                            {this.renderErrorFor("meio_contato")}
                                        </div>
                                        <div className="form-group col-12">
                                            <textarea rows="5" placeholder="Mensagem" name="mensagem" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("mensagem") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("mensagem") : this.state.mensagem} />
                                            {this.renderErrorFor("mensagem")}
                                        </div>
                                        <div className="form-group offset-6 col-6">
                                            <button>Enviar mensagem</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_settings: state.settings.get("list_settings"),
    error_contato: state.contatos.get("error_contato"),
    success_contato: state.contatos.get("success_contato"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({sendContato}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Contato);
