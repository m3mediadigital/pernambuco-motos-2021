import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import Loader from "react-loader-spinner";
import MonteBox from "../elements/MonteBox";
import InputMask from "react-input-mask";
import {getListNovas} from "../../actions/NovasAction";
import {sendContato} from "../../actions/ContatosAction";
import Swal from "sweetalert2";
import ScriptsSend from "../helpers/ScriptsSend";
const horas = [
     '08:00', '09:00', '10:00', '11:00', '12:00',
     '13:00', '14:00', '15:00', '16:00', '17:00', '18:00'
]
class TesteDrive extends Component {
    constructor(props) {
        super(props)
        this.state = {
            page: 1,
            section: 1,

            vehicle: '',
            vehicle_name: null,
            vehicle_image: '',

            hora_1: '',
            data_1: '',
            hora_2: '',
            data_2: '',

            nome: "",
            email: "",
            pcd: "",
            telefone: "",
            mensagem: "",
            meio_contato: "",
            sending: false,
        };
        this.getListNovas();
        this.method = "send-contato";
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    getListNovas() {
        this.props.getListNovas(null, 0);
    }
    setPage(page) {
        if(page < this.state.page){
            this.setState({page: page})
        }
    }
    nextPage() {
        if (this.state.page === 1) {
            if (this.state.vehicle.length > 0) {
                this.setState({page: 2})
            } else {
                Swal.fire({
                    title: 'Erro!',
                    text: 'Selecione um modelo!',
                    type: 'error'
                })
            }
        }
        if (this.state.page === 2) {
            if (
                this.state.data_1.length > 0 && this.state.hora_1.length > 0 &&
                this.state.data_2.length > 0 && this.state.hora_2.length > 0

            ) {
                this.setState({page: 3})
            } else {
                Swal.fire({
                    title: 'Erro!',
                    text: 'Selecione os valores da parcela!',
                    type: 'error'
                })
            }
        }

    }
    clearForm(field) {
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state[field] = field === "sending" ? false : "";
        return this.state[field];
    }
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    }
    hasErrorFor(field) {
        return !!this.props.error_contato.get(field);
    }
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state["sending"] = false;
            return (
                <span className="error">
                    <strong>{this.props.error_contato.get(field).get(0)}</strong>
                </span>
            );
        }
    }
    handleSubmit(event) {
        let msg =
            "Data 1: " + this.state.data_1 + " - " +
            "Horario 1 : " + this.state.hora_1 + " - " +
            "Data 2 : " + this.state.data_2 + " - " +
            "Horario 2 : " + this.state.hora_2 + " - " +
            "Veículo : " + this.state.vehicle_name;
        this.setState({mensagem: msg});

        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
        });

        const formData = new FormData();
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", "teste_drive");
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("pcd", this.state.pcd);
        formData.append("mensagem", this.state.mensagem);
        formData.append("assunto", "Formulario de Teste Drive");
        formData.append("unidade", "Matriz");
        formData.append("meio_contato", this.state.meio_contato);

        this.props.sendContato(this.method, formData);
    }
    render() {
        const {list_novas, success_contato} = this.props;
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <h1>Agende seu Teste Drive</h1>
                            <p>Agende seu teste drive de forma rápido e sinta na pele o seu Mitsubishi</p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid">
                    <div className="container-fluid tabs-holder">
                        <div className="container">
                            <div className="row">
                                <div className={"col-3 tab " + (this.state.page === 1 ? " active" : "")} onClick={() => this.setPage(1)}>
                                    <h1>1.</h1>
                                    <p>Escolha o modelo</p>
                                </div>
                                <div className={"col-3 tab " + (this.state.page === 2 ? " active" : "")} onClick={() => this.setPage(2)}>
                                    <h1>2.</h1>
                                    <p>Disponibilidade</p>
                                </div>
                                <div className={"col-3 tab " + (this.state.page === 3 ? " active" : "")} onClick={() => this.setPage(3)}>
                                    <h1>3.</h1>
                                    <p>Identificação</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid bg-white">
                        <div className={"container page_1 " + (this.state.page === 1 ? " " : "d-none")}>
                            <div className="row">
                                {list_novas.size === 0 ? (
                                    <div className="loading mt-5 mb-5">
                                        <Loader type="Oval" color="#DB0000" height={100} width={100}/>
                                    </div>
                                ) : (
                                    list_novas.map((item) => {
                                        return item ? <MonteBox
                                            slug={item.get('slug')}
                                            title={item.getIn(['model', 'complete_name'])}
                                            image={item.getIn(['model', 'image'])}
                                            selected={this.state.vehicle === item.get('slug')}
                                            onClick={() => {
                                                this.setState({vehicle: item.get('slug')})
                                                this.setState({vehicle_image: item.getIn(['model', 'image'])})
                                                this.setState({vehicle_name: item.getIn(['model', 'complete_name'])})
                                            }}
                                        /> : null;
                                    })
                                )}
                            </div>
                        </div>

                        <div className={"container page_2 " + (this.state.page === 2 ? " " : "d-none")}>
                            <div className="row">
                                <div className="col-6 pt-5 pb-5">
                                    <div className="row">
                                        <div className="col-12">
                                            <strong>
                                               Quando deseja realizar o teste?
                                            </strong>
                                        </div>
                                        <div className="form-group col-6">
                                            <select name="hora_1" id="" className={`form-control ${this.hasErrorFor("hora_1") ? "error" : ""}`} onChange={this.handleChange}>
                                                <option value="">Hora 1</option>
                                                {horas.map((item, index) => (
                                                    <option key={index} value={item}>
                                                        {item}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="form-group col-6">
                                            <select name="hora_2" id="" className={`form-control ${this.hasErrorFor("hora_2") ? "error" : ""}`} onChange={this.handleChange}>
                                                <option value="">Hora 2</option>
                                                {horas.map((item, index) => (
                                                    <option key={index} value={item}>
                                                        {item}
                                                    </option>
                                                ))}
                                            </select>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <InputMask mask="99/99/99" type="text" name="data_1"
                                                           className={`form-control ${this.hasErrorFor("data_1") ? "error" : ""}`}
                                                           value={success_contato.size > 0 ? this.clearForm("data_1") : this.state.data_1}
                                                           placeholder="Data 1"
                                                           onChange={this.handleChange} />
                                            </label>
                                        </div>
                                        <div className="form-group col-6">
                                            <label>
                                                <InputMask mask="99/99/99" type="text" name="data_2"
                                                           className={`form-control ${this.hasErrorFor("data_2") ? "error" : ""}`}
                                                           value={success_contato.size > 0 ? this.clearForm("data_2") : this.state.data_2}
                                                           placeholder="Data 2"
                                                           onChange={this.handleChange} />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-6 pt-5 ">
                                    <img src={this.state.vehicle_image} alt="" className="img-fluid"/>
                                </div>
                            </div>
                        </div>

                        <div className={"container page_3 " + (this.state.page === 3 ? "" : "d-none")}>
                            <div className="row">
                                <div className="col-12 col-lg-6">
                                    <form className="container mt-5 mb-5" onSubmit={this.handleSubmit} method="post">
                                        <div className="row">
                                            <div className="form-group col-12 ">
                                                <input type="text" placeholder="Nome e Sobrenome" className={`form-control ${this.hasErrorFor("nome") ? "error" : ""}`} name="nome" value={success_contato.size > 0 ? this.clearForm("nome") : this.state.nome} onChange={this.handleChange} />
                                                {this.renderErrorFor("nome")}
                                            </div>
                                            <div className="form-group col-12 col-lg-8">
                                                <InputMask mask="(99) 99999-9999" type="text" name="telefone" className={`form-control ${this.hasErrorFor("telefone") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("telefone") : this.state.telefone} placeholder="Telefone" onChange={this.handleChange} />
                                                {this.renderErrorFor("telefone")}
                                            </div>
                                            <div className="form-group col-12 col-lg-4">
                                                <select name="pcd" id="pcd" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("pcd") ? "error" : ""}`}>
                                                    <option value="">PCD?</option>
                                                    <option value="Sim">Sim</option>
                                                    <option value="Nao">Nao</option>
                                                </select>
                                                {this.renderErrorFor("pcd")}
                                            </div>
                                            <div className="form-group col-12">
                                                <input type="text" name="email" placeholder="Email" className={`form-control ${this.hasErrorFor("email") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("email") : this.state.email} onChange={this.handleChange} />
                                                {this.renderErrorFor("email")}
                                            </div>
                                            <div className="form-group col-12">
                                                <select name="meio_contato" id="meio_contato" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("meio_contato") ? "error" : ""}`}>
                                                    <option value="">Por onde deseja ser contatado?</option>
                                                    <option value="Email">Email</option>
                                                    <option value="Telefone">Telefone</option>
                                                    <option value="Whatsapp">Whatsapp</option>
                                                </select>
                                                {this.renderErrorFor("meio_contato")}
                                            </div>

                                            <div className="form-group offset-6 col-6">
                                                <button className="btn-send">Enviar mensagem</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="col-6 pt-5 ">
                                    <img src={this.state.vehicle_image} alt="" className="img-fluid"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container-fluid tabs-holder">
                        <div className="container">
                            <div className="row">
                                <div className="col-3 align-content-center d-flex flex-column">
                                    <span className="mt-4">Modelo Selecionado</span>
                                    <strong>{this.state.vehicle_name ? this.state.vehicle_name : '--'} </strong>
                                </div>
                                <div className="col-3 align-content-center d-flex flex-column">
                                    <span className="mt-4">Data 1</span>
                                    <strong>{this.state.data_1 && this.state.hora_1 ? this.state.data_1 + ' - ' + this.state.hora_1 : '--'}</strong>
                                </div>
                                <div className="col-3 align-content-center d-flex flex-column">
                                    <span className="mt-4">Data 2</span>
                                    <strong>{this.state.data_2 && this.state.hora_2 ? this.state.data_2 + ' - ' + this.state.hora_2 : '--'}</strong>
                                </div>
                                <div className="col-3 align-content-center d-flex flex-column justify-content-center pt-4">
                                    <button className={"btn-send " + (this.state.page === 4 ? 'd-none': '')} onClick={() => this.nextPage()}>Avançar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_novas: state.novas.get("list_novas"),
    list_settings: state.settings.get("list_settings"),
    error_contato: state.contatos.get("error_contato"),
    success_contato: state.contatos.get("success_contato"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListNovas, sendContato}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(TesteDrive);
