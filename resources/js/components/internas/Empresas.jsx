import React, { Component } from "react";
import Main from "../Main";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import iconlink from "../../assets/images/icon-link.png";
import autovema_fiat from "../../assets/images/empresas/autovema_fiat.jpg";
import autovema_motors from "../../assets/images/empresas/autovema_motors.png";
import card_ideal from "../../assets/images/empresas/card_ideal.jpg";
import ecogear from "../../assets/images/empresas/ecogear.png";
import grupo_rovema from "../../assets/images/empresas/grupo_rovema.png";
import madeira_seguros from "../../assets/images/empresas/madeira_seguros.jpg";
import remopecas from "../../assets/images/empresas/remopecas.jpg";
import rovema_energia from "../../assets/images/empresas/rovema_energia.jpg";
import rovema_agro from "../../assets/images/empresas/rovema_agro.jpg";
import rovema_locadora from "../../assets/images/empresas/rovema_locadora.png";
import rovema_veiculos_maquinas from "../../assets/images/empresas/rovema_veiculos_maquinas.png";
import uzzipay from "../../assets/images/empresas/uzzipay.jpg";

import { getInstitucional } from "../../actions/InstitucionalAction";

class QuemSomos extends Component {
    constructor(props) {
        super(props);

        this.props.getInstitucional();
    }
    render() {
        const { list_institucional } = this.props;
        console.log(list_institucional);
        return (
            <Main>
                <div className="empresa">
                    <div className="empresa-box pt-5 pb-5">
                        <div className="container">
                            <h3 className="pb-3">Grupo Rovema</h3>
                            {list_institucional &&
                                list_institucional.map((item) => {
                                    if (item.get("slug") === "quem-somos") {
                                        return (
                                            <>
                                                <div dangerouslySetInnerHTML={{ __html: item.get("conteudo") }} />
                                            </>
                                        );
                                    }
                                })}
                        </div>
                    </div>
                    <div className="empresa-content">
                        <div className="container">
                            <p className="text-center  m-0">São mais de 10 empresas do grupo em diferentes ramos e áreas de atuação</p>
                            <h4 className="text-center text-uppercase">
                                <strong>conheça cada uma delas.</strong>
                            </h4>
                            <div className="row">
                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={card_ideal} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Card title</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.cardideal.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={rovema_agro} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Rovema Agronegócios</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.rovemaagronegocio.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={autovema_motors} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Autovema Motors</h5>
                                                </li>
                                                <li>
                                                    <a href="https://autovemamotors.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={ecogear} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Ecogear</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.idealsolucoesambientais.com.br" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={grupo_rovema} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Grupo Rovema</h5>
                                                </li>
                                                <li>
                                                    <a href="https://www.gruporovema.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={uzzipay} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">UzziPay</h5>
                                                </li>
                                                <li>
                                                    <a href="https://uzzipay.com/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={remopecas} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">RemoPeças</h5>
                                                </li>
                                                <li>
                                                    <a href="https://www.remopecas.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={rovema_energia} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Rovema Energia</h5>
                                                </li>
                                                <li>
                                                    <a href="http://rovemaenergia.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={rovema_veiculos_maquinas} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Rovema Veículos e Máquinas</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.rovema.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={autovema_fiat} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Autovema Fiat</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.autovemafiat.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={madeira_seguros} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Madeiras Seguros</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.madeiraseguros.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>

                                <div className="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 pb-4">
                                    <div class="card w-100 d-flex justify-content-center align-items-center">
                                        <img src={rovema_locadora} class="card-img-top" alt="card ideal" />
                                        <div class="card-body w-100 position-absolute d-flex justify-content-center">
                                            <ul className="text-center">
                                                <li>
                                                    <h5 class="card-title">Rovema Locadora</h5>
                                                </li>
                                                <li>
                                                    <a href="http://www.rovemalocadora.com.br/" target="_blank" rel="noopener noreferrer">
                                                        <img src={iconlink} alt="Link" />
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="bg position-absolute"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_institucional: state.institucional.get("list_institucional"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ getInstitucional }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(QuemSomos);
