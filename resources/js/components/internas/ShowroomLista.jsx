import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";
import ShowroomBox from "../elements/ShowroomBox";
import {getListNovasIndex} from "../../actions/NovasAction";
import Loader from "react-loader-spinner";

class ShowroomLista extends Component {
    constructor(props) {
        super(props);

        this.getListNovasIndex();
    }

    getListNovasIndex() {
        this.props.getListNovasIndex(null, 0);
    }
    render() {
        const {list_novas} = this.props;
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Quem conhece" subtitle="tira o chapéu" />
                        </div>
                        {list_novas.size === 0 ? (
                            <div className="loading">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                            </div>
                        ) : (
                            list_novas.map((item, index) => {
                                return item ? <ShowroomBox item={item} key={index} /> : null;
                            })
                        )}
                    </div>
                </div>
                <div className="container"></div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_novas: state.novas.get("list_novas"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListNovasIndex}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ShowroomLista);
