import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import TitleSection from "../elements/TitleSection";
import PartBox from "../elements/PartBox";
import Want from "../sections/Want";
import Loader from "react-loader-spinner";
import {getListAcessorios} from "../../actions/AcessoriosAction";

class Acessorios extends Component {
    constructor(props) {
        super(props);
        this.getListAcessorios();
    }

    getListAcessorios() {
        this.props.getListAcessorios(null, 0);
    }

    render() {
        const {list_acessorios} = this.props;
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Peças genuínas" subtitle="Honda" />
                        </div>

                        {list_acessorios.size === 0 ? (
                            <div className="loading">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                            </div>
                        ) : (
                            <PartBox items={list_acessorios} />
                        )}
                    </div>
                </div>
                <div className="container">
                    <Want />
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_acessorios: state.acessorios.get("list_acessorios"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListAcessorios}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Acessorios);
