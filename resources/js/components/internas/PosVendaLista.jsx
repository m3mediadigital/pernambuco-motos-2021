import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";
import PosVendaBox from "../elements/PosVendaBox";

import {getListAcessorios} from "../../actions/AcessoriosAction";
import Loader from "react-loader-spinner";

class Servicos extends Component {
    constructor(props) {
        super(props);
        this.getListAcessorios();
    }

    getListAcessorios() {
        this.props.getListAcessorios(null, 0);
    }

    render() {
        const {list_acessorios} = this.props;
        return (
            <Main>
                <div className="container page Acessorios">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Serviços premium" subtitle="Honda" />
                        </div>
                        {list_acessorios.size === 0 ? (
                            <div className="loading p-5">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                            </div>
                        ) : (
                            <PosVendaBox items={list_acessorios} />
                        )}
                    </div>
                </div>
                <div className="container"></div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_acessorios: state.acessorios.get("list_acessorios"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListAcessorios}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Servicos);
