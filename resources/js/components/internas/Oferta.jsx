import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {getOferta} from "../../actions/OfertasAction";
import OfertaItem from "../elements/OfertaItem";
import Loader from "react-loader-spinner";

class Oferta extends Component {
    constructor(props) {
        super(props);
        const {slug} = this.props.match.params;

        if (slug) {
            this.props.getOferta(slug);
        }
    }
    render() {
        const {item_oferta} = this.props;
        return (
            <Main>
                <div className="container seminovo page">
                    {item_oferta != null && item_oferta.size > 0 ? (
                        <OfertaItem item_oferta={item_oferta} />
                    ) : (
                        <div className="loading text-center">
                            <Loader type="Oval" color="#DB0000" height={100} width={100} />
                        </div>
                    )}
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    item_oferta: state.ofertas.get("item_oferta"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getOferta}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Oferta);
