import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

const states = [
    {key: "AC", value: "Acre"},
    {key: "AL", value: "Alagoas"},
    {key: "AM", value: "Amazonas"},
    {key: "AP", value: "Amapá"},
    {key: "BA", value: "Bahia"},

    {key: "CE", value: "Ceará"},
    {key: "DF", value: "Distrito Federal"},
    {key: "ES", value: "Espírito Santo"},
    {key: "GO", value: "Goiás"},
    {key: "MA", value: "Maranhão"},

    {key: "MG", value: "Minas Gerais"},
    {key: "MS", value: "Mato Grosso do Sul"},
    {key: "MT", value: "Mato Grosso"},
    {key: "PA", value: "Pará"},
    {key: "PB", value: "Paraíba"},

    {key: "PE", value: "Pernambuco"},
    {key: "PI", value: "Piauí"},
    {key: "PR", value: "Paraná"},
    {key: "RJ", value: "Rio de Janeiro"},
    {key: "RN", value: "Rio Grande do Norte"},

    {key: "RO", value: "Rondônia"},
    {key: "RR", value: "Roraima"},
    {key: "RS", value: "Rio Grande do Sul"},
    {key: "SC", value: "Santa Catarina"},
    {key: "SE", value: "Sergipe"},

    {key: "SP", value: "São Paulo"},
    {key: "TO", value: "Tocantins"},
];

class AnaliseCredito extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            section: 1,
        };
    }

    render() {
        return (
            <Main>
                <div className="container analise-credito page">
                    <div className="row">
                        <div className="col-12">
                            <h1>Análise de crédito</h1>
                            <p>Quer financiar um Mitsubishi? Entrar um de nossos consórcio? Faça uma análise de crédito!</p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid tabs-holder">
                    <div className="container">
                        <div className="row">
                            <div className={"col-3 tab " + (this.state.page === 1 ? " active" : "")}>
                                <h1>1.</h1>
                                <p>Sobre o cliente</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 2 ? " active" : "")}>
                                <h1>2.</h1>
                                <p>Conjuge ou Avalista</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 3 ? " active" : "")}>
                                <h1>3.</h1>
                                <p>Referencia Bancaria</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 4 ? " active" : "")}>
                                <h1>4.</h1>
                                <p>Finalizar</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container-fluid bg-white">
                    <div className={"container page_1 " + (this.state.page === 1 ? " " : "d-none")}>
                        <div className="section-tabs row">
                            <div className={"section-tab col-4 " + (this.state.section === 1 ? " active" : "")} onClick={() => this.setState({section: 1})}>
                                <strong>Dados Pessoais</strong>
                                <i className="fa fa-caret-right"></i>
                            </div>
                            <div className={"section-tab col-4 " + (this.state.section === 2 ? " active" : "")} onClick={() => this.setState({section: 2})}>
                                <strong>Endereço e contato</strong>
                                <i className="fa fa-caret-right"></i>
                            </div>
                            <div className={"section-tab col-4 " + (this.state.section === 3 ? " active" : "")} onClick={() => this.setState({section: 3})}>
                                <strong>Fonte de Renda</strong>
                            </div>
                        </div>
                        <div className={"row section-1 " + (this.state.section === 1 ? " d-flex" : "d-none")}>
                            <div className="col-12">
                                <br />
                                <p>Todos os campos são obrigatórios</p>
                                <br />
                            </div>
                            <div className="form row col-12">
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome Completo" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Nascimento" />
                                </div>
                                <div className="col-lg-3 form-group">
                                    <select name="" id="" className="form-control">
                                        <option value="">Estado Civil</option>
                                        <option value="">Casado</option>
                                        <option value="">Solteiro</option>
                                        <option value="">Divorciado</option>
                                    </select>
                                </div>
                                <div className="col-lg-2 form-group">
                                    <select name="" id="" className="form-control">
                                        <option value="">Sexo</option>
                                        <option value="">Masculino</option>
                                        <option value="">Feminino</option>
                                        <option value="">Não binario</option>
                                    </select>
                                </div>

                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Naturalidade" />
                                </div>
                                <div className="col-lg-3 form-group">
                                    <input type="text" className="form-control" placeholder="Nacionalidade" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="CPF" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="RG" />
                                </div>
                                <div className="col-lg-3 form-group">
                                    <input type="text" className="form-control" placeholder="Orgão Emissor" />
                                </div>

                                <div className="col-lg-2 form-group">
                                    <select name="" id="" className="form-control">
                                        <option value="">PCD?</option>
                                        <option value="">Sim</option>
                                        <option value="">Não</option>
                                    </select>
                                </div>
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome do Pai" />
                                </div>
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome da Mãe" />
                                </div>
                            </div>
                        </div>
                        <div className={"row section-2 " + (this.state.section === 2 ? " d-flex" : "d-none")}>
                            <div className="col-12">
                                <br />
                                <p>Todos os campos são obrigatórios</p>
                                <br />
                            </div>
                            <div className="form row col-12">
                                <div className="col-lg-10 form-group">
                                    <input type="text" className="form-control" placeholder="Endereço" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Numero" />
                                </div>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Cidade" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Bairro" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Estado</option>
                                    {states.map((item, index) => (
                                        <option key={index} value={item.key}>
                                            {item.value}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="CEP" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Tipo Residencia</option>
                                    <option value="">Casa</option>
                                    <option value="">Apartamento</option>
                                    <option value="">Rural</option>
                                </select>
                            </div>
                            <div className="col-lg-3 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Tempo de residencia</option>
                                    <option value="">1 ano</option>
                                    <option value="">2 anos</option>
                                    <option value="">3 ou mais</option>
                                </select>
                            </div>
                            <div className="col-12">
                                <br />
                                <p>Informações de contato</p>
                                <br />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Nome para contato" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Telefone residencial (opcional)" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Celular/Whatsapp" />
                            </div>
                        </div>
                        <div className={"row section-3 " + (this.state.section === 3 ? " d-flex" : "d-none")}>
                            <div className="col-12">
                                <br />
                                <p>Todos os campos são obrigatórios</p>
                                <br />
                            </div>
                            <div className="col-lg-8 form-group">
                                <input type="text" className="form-control" placeholder="Razão social/Nome Fantasia" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="CNPJ" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="Cargo/Função" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="Data de admissão" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="Salario" />
                            </div>
                            <div className="col-12">
                                <br />
                                <strong>Endereço da Empresa</strong>
                                <br />
                            </div>
                            <div className="form row col-12">
                                <div className="col-lg-10 form-group">
                                    <input type="text" className="form-control" placeholder="Endereço" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Numero" />
                                </div>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Cidade" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Bairro" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Estado</option>
                                    {states.map((item, index) => (
                                        <option key={index} value={item.key}>
                                            {item.value}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="CEP" />
                            </div>
                        </div>
                    </div>
                    <div className={"container page_2 " + (this.state.page === 2 ? " " : "d-none")}>
                        <div className="section-tabs row">
                            <div className={"section-tab col-4 " + (this.state.section === 1 ? " active" : "")} onClick={() => this.setState({section: 1})}>
                                <strong>Dados Pessoais</strong>
                                <i className="fa fa-caret-right"></i>
                            </div>
                            <div className={"section-tab col-4 " + (this.state.section === 2 ? " active" : "")} onClick={() => this.setState({section: 2})}>
                                <strong>Fonte de Renda</strong>
                            </div>
                        </div>
                        <div className={"row section-1 " + (this.state.section === 1 ? " d-flex" : "d-none")}>
                            <div className="col-12">
                                <br />
                                <p>Todos os campos são obrigatórios</p>
                                <br />
                            </div>
                            <div className="form row col-12">
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome Completo" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Nascimento" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <select name="" id="" className="form-control">
                                        <option value="">Sexo</option>
                                        <option value="">Masculino</option>
                                        <option value="">Feminino</option>
                                        <option value="">Não binario</option>
                                    </select>
                                </div>

                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Naturalidade" />
                                </div>
                                <div className="col-lg-3 form-group">
                                    <input type="text" className="form-control" placeholder="Nacionalidade" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="CPF" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="RG" />
                                </div>
                                <div className="col-lg-3 form-group">
                                    <input type="text" className="form-control" placeholder="Orgão Emissor" />
                                </div>

                                <div className="col-lg-2 form-group">
                                    <select name="" id="" className="form-control">
                                        <option value="">PCD?</option>
                                        <option value="">Sim</option>
                                        <option value="">Não</option>
                                    </select>
                                </div>
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome do Pai" />
                                </div>
                                <div className="col-lg-5 form-group">
                                    <input type="text" className="form-control" placeholder="Nome da Mãe" />
                                </div>
                            </div>
                        </div>
                        <div className={"row section-2 " + (this.state.section === 2 ? " d-flex" : "d-none")}>
                            <div className="col-12">
                                <br />
                                <p>Todos os campos são obrigatórios</p>
                                <br />
                            </div>
                            <div className="col-lg-8 form-group">
                                <input type="text" className="form-control" placeholder="Razão social/Nome Fantasia" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Tempo de serviço</option>
                                    <option value="">1 ano</option>
                                    <option value="">2 anos</option>
                                    <option value="">3 anos</option>
                                    <option value="">4 anos</option>
                                    <option value="">5 anos ou mais</option>
                                </select>
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="Cargo/Função" />
                            </div>
                            <div className="col-lg-4 form-group">
                                <input type="text" className="form-control" placeholder="Salário/Renda" />
                            </div>
                            <div className="col-12">
                                <br />
                                <strong>Endereço da Empresa</strong>
                                <br />
                            </div>
                            <div className="form row col-12">
                                <div className="col-lg-10 form-group">
                                    <input type="text" className="form-control" placeholder="Endereço Comercial" />
                                </div>
                                <div className="col-lg-2 form-group">
                                    <input type="text" className="form-control" placeholder="Número" />
                                </div>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Cidade" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="Bairro" />
                            </div>
                            <div className="col-lg-3 form-group">
                                <select name="" id="" className="form-control">
                                    <option value="">Estado</option>
                                    {states.map((item, index) => (
                                        <option key={index} value={item.key}>
                                            {item.value}
                                        </option>
                                    ))}
                                </select>
                            </div>
                            <div className="col-lg-3 form-group">
                                <input type="text" className="form-control" placeholder="CEP" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid tabs-holder">
                    <div className="container">
                        <div className="row">
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Nome do cliente</span>
                                <strong> -- </strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Idade</span>
                                <strong> -- </strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Contato</span>
                                <strong>---</strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column justify-content-center pt-4">
                                <button className="btn-send">Avançar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AnaliseCredito);
