import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import MonteBox from "../elements/MonteBox";
import {getListNovas} from "../../actions/NovasAction";
import Loader from "react-loader-spinner";
import Swal from "sweetalert2";
import InputMask from "react-input-mask";
import ScriptsSend from "../helpers/ScriptsSend";
import {sendContato} from "../../actions/ContatosAction";

class MonteSeu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 1,
            section: 1,

            vehicle: '',
            vehicle_name: null,

            version: '',
            version_name: null,

            color_image: null,
            color_name: '',
            hexadecimal: null,

            nome: "",
            email: "",
            pcd: "",
            telefone: "",
            mensagem: "",
            meio_contato: "",
            sending: false,
        };
        this.getListNovas();
        this.method = "send-contato";
        this.hasErrorFor = this.hasErrorFor.bind(this);
        this.renderErrorFor = this.renderErrorFor.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    getListNovas() {
        this.props.getListNovas(null, 0);
    }
    setPage(page) {
        if(page < this.state.page){
            this.setState({page: page})
        }
        // eslint-disable-next-line default-case
        switch (page) {
            case 1:
                this.setState({
                    vehicle: '',
                    vehicle_name: null,
                    version: '',
                    color_name: ''
                })
                break;
            case 2:
                this.setState({
                    version: '',
                    version_name: null,
                    color_name: ''
                })
                break;
            case 3:
                this.setState({color_name: ''})
                break;
        }
    }
    nextPage() {
        if (this.state.page === 1) {
            if (this.state.vehicle.length > 0) {
                this.setState({page: 2})
            } else {
                Swal.fire({
                    title: 'Erro!',
                    text: 'Selecione um modelo!',
                    type: 'error'
                })
            }
        }
        if (this.state.page === 2) {
            if (this.state.version.length > 0) {
                this.setState({page: 3})
            } else {
                Swal.fire({
                    title: 'Erro!',
                    text: 'Selecione uma versão!',
                    type: 'error'
                })
            }
        }
        if (this.state.page === 3) {
            if (this.state.color_name.length > 0) {
                this.setState({page: 4})
            } else {
                Swal.fire({
                    title: 'Erro!',
                    text: 'Selecione uma cor!',
                    type: 'error'
                })
            }
        }
    }
    clearForm(field) {
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state[field] = field === "sending" ? false : "";
        return this.state[field];
    }
    handleChange(event) {
        const {name, value} = event.target;
        this.setState({
            [name]: value,
        });
    }
    hasErrorFor(field) {
        return !!this.props.error_contato.get(field);
    }
    renderErrorFor(field) {
        if (this.hasErrorFor(field)) {
            // eslint-disable-next-line react/no-direct-mutation-state
            this.state["sending"] = false;
            return (
                <span className="error">
                    <strong>{this.props.error_contato.get(field).get(0)}</strong>
                </span>
            );
        }
    }
    handleSubmit(event) {
        event.preventDefault();
        const {list_settings} = this.props;
        this.setState({
            sending: true,
        });

        const formData = new FormData();
        formData.append("to_address", ScriptsSend.getEmailsByKey(list_settings, this.method));
        formData.append("form", "monte");
        formData.append("nome", this.state.nome);
        formData.append("email", this.state.email);
        formData.append("telefone", this.state.telefone);
        formData.append("pcd", this.state.pcd);
        formData.append("mensagem", this.state.mensagem);
        formData.append("assunto", "Formulario de Monte o Seu");
        formData.append("unidade", "Matriz");
        formData.append("meio_contato", this.state.meio_contato);

        this.props.sendContato(this.method, formData);
    }
    render() {
        const {list_novas, success_contato} = this.props;
        return (
            <Main>
                <div className="container  page">
                    <div className="row">
                        <div className="col-12">
                            <h1>Monte o seu</h1>
                            <p>Seu Mitsubishi, seu estilo.</p>
                        </div>
                    </div>
                </div>
                <div className="container-fluid tabs-holder">
                    <div className="container">
                        <div className="row">
                            <div className={"col-3 tab " + (this.state.page === 1 ? " active" : "")} onClick={() => this.setPage(1)}>
                                <h1>1.</h1>
                                <p>Escolha o modelo</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 2 ? " active" : "")} onClick={() => this.setPage(2)}>
                                <h1>2.</h1>
                                <p>Escolha a versão</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 3 ? " active" : "")} onClick={() => this.setPage(3)}>
                                <h1>3.</h1>
                                <p>Escolha a cor</p>
                            </div>
                            <div className={"col-3 tab " + (this.state.page === 4 ? " active" : "")}>
                                <h1>4.</h1>
                                <p>Solicitar cotação</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid bg-white">
                    <div className={"container page_1 " + (this.state.page === 1 ? " " : "d-none")}>
                        <div className="row">
                            {list_novas.size === 0 ? (
                                <div className="loading mt-5 mb-5">
                                    <Loader type="Oval" color="#DB0000" height={100} width={100}/>
                                </div>
                            ) : (
                                list_novas.map((item) => {
                                    return item ? <MonteBox
                                        slug={item.get('slug')}
                                        title={item.getIn(['model', 'complete_name'])}
                                        image={item.getIn(['model', 'image'])}
                                        selected={this.state.vehicle === item.get('slug')}
                                        onClick={() => {
                                            this.setState({vehicle: item.get('slug')})
                                            this.setState({vehicle_name: item.getIn(['model', 'complete_name'])})
                                        }}
                                    /> : null;
                                })
                            )}
                        </div>
                    </div>

                    <div className={"container page_2 " + (this.state.page === 2 ? " " : "d-none")}>
                        <div className="row">
                            {list_novas.size === 0 ? (
                                <div className="loading mt-5 mb-5">
                                    <Loader type="Oval" color="#DB0000" height={100} width={100}/>
                                </div>
                            ) : (
                                list_novas.map((item) =>
                                    item.get("versoes").map((versao) => {

                                        return versao ? <MonteBox
                                            classname={(this.state.vehicle === item.get('slug') ? '' : 'd-none')}
                                            slug={versao.get('slug')}
                                            title={versao.get('complete_name')}
                                            subtitle={item.getIn(['model', 'complete_name'])}
                                            image={versao.get('image')}
                                            selected={this.state.version === versao.get('slug')}
                                            onClick={() => {
                                                this.setState({version: versao.get('slug')})
                                                this.setState({version_name: versao.get('complete_name')})
                                                this.setState({color_image: versao.get('image') })
                                            }}
                                        /> : null;
                                    })
                                )
                            )}
                        </div>
                    </div>
                    <div className={"container page_3 " + (this.state.page === 3 ? " " : "d-none")}>
                        <div className="row">
                            <div className="col-12 pb-5 pt-5">
                                {list_novas.size === 0 ? (
                                    <div className="loading mt-5 mb-5">
                                        <Loader type="Oval" color="#DB0000" height={100} width={100}/>
                                    </div>
                                ) : (
                                    list_novas.map((item) =>
                                        item.get("versoes").map((versao) => (
                                            <div className={'itemColor ' + (this.state.version === versao.get('slug') ? '' : 'd-none')} key={versao.get('id')}>
                                                <img  src={this.state.color_image ? this.state.color_image : '' } alt="" className="color_image"/>
                                                <div className="colors-line">
                                                {versao.get('colors').map((cor) => (
                                                    <div className="color_holder" onClick={() => {
                                                        this.setState({hexadecimal: cor.get('hexadecimal') })
                                                        this.setState({color_name: cor.get('color') })
                                                        this.setState({color_image: cor.get('image') })
                                                    }}>
                                                        <div className="color_box" style={{backgroundColor: cor.get('hexadecimal')}}>
                                                            <i className={"fa fa-check " + (this.state.hexadecimal === cor.get('hexadecimal') ? '' : 'd-none')}  />
                                                        </div>
                                                        <div className={"color_text " + (this.state.hexadecimal === cor.get('hexadecimal') ? '' : 'd-none')}>
                                                            {cor.get('color')}
                                                        </div>
                                                    </div>
                                                ))}
                                                </div>
                                            </div>
                                            )
                                        )
                                    )
                                )}
                            </div>
                        </div>
                    </div>
                    <div className={"container page_4 " + (this.state.page === 4 ? "" : "d-none")}>
                        <div className="row">
                            <div className="col-12 col-lg-6">
                                <form className="container mt-5 mb-5" onSubmit={this.handleSubmit} method="post">
                                    <div className="row">
                                        <div className="form-group col-12 ">
                                            <input type="text" placeholder="Nome e Sobrenome" className={`form-control ${this.hasErrorFor("nome") ? "error" : ""}`} name="nome" value={success_contato.size > 0 ? this.clearForm("nome") : this.state.nome} onChange={this.handleChange} />
                                            {this.renderErrorFor("nome")}
                                        </div>
                                        <div className="form-group col-12 col-lg-8">
                                            <InputMask mask="(99) 99999-9999" type="text" name="telefone" className={`form-control ${this.hasErrorFor("telefone") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("telefone") : this.state.telefone} placeholder="Telefone" onChange={this.handleChange} />
                                            {this.renderErrorFor("telefone")}
                                        </div>
                                        <div className="form-group col-12 col-lg-4">
                                            <select name="pcd" id="pcd" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("pcd") ? "error" : ""}`}>
                                                <option value="">PCD?</option>
                                                <option value="Sim">Sim</option>
                                                <option value="Nao">Nao</option>
                                            </select>
                                            {this.renderErrorFor("pcd")}
                                        </div>
                                        <div className="form-group col-12">
                                            <input type="text" name="email" placeholder="Email" className={`form-control ${this.hasErrorFor("email") ? "error" : ""}`} value={success_contato.size > 0 ? this.clearForm("email") : this.state.email} onChange={this.handleChange} />
                                            {this.renderErrorFor("email")}
                                        </div>
                                        <div className="form-group col-12">
                                            <select name="meio_contato" id="meio_contato" onChange={this.handleChange} className={`form-control ${this.hasErrorFor("meio_contato") ? "error" : ""}`}>
                                                <option value="">Por onde deseja ser contatado?</option>
                                                <option value="Email">Email</option>
                                                <option value="Telefone">Telefone</option>
                                                <option value="Whatsapp">Whatsapp</option>
                                            </select>
                                            {this.renderErrorFor("meio_contato")}
                                        </div>

                                        <div className="form-group offset-6 col-6">
                                            <button className="btn-send">Enviar mensagem</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="col-12 col-lg-6 pt-5">
                                <img src={this.state.color_image} alt="" className="img-fluid"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid tabs-holder">
                    <div className="container">
                        <div className="row">
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Modelo Selecionado</span>
                                <strong>{this.state.vehicle_name ? this.state.vehicle_name : '--'} </strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Versão</span>
                                <strong>{this.state.version_name ? this.state.version_name : '--'}</strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column">
                                <span className="mt-4">Cor</span>
                                <strong>{this.state.color_name ? this.state.color_name : '--'}</strong>
                            </div>
                            <div className="col-3 align-content-center d-flex flex-column justify-content-center pt-4">
                                <button className={"btn-send " + (this.state.page === 4 ? 'd-none': '')} onClick={() => this.nextPage()}>Avançar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_novas: state.novas.get("list_novas"),
    list_settings: state.settings.get("list_settings"),
    error_contato: state.contatos.get("error_contato"),
    success_contato: state.contatos.get("success_contato"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListNovas, sendContato}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MonteSeu);
