import React, {Component} from "react";
import Main from "../Main";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import TitleSection from "../elements/TitleSection";
import OfertasBox from "../elements/OfertasBox";
import {getListOfertas} from "../../actions/OfertasAction";

import Loader from "react-loader-spinner";

class OfertasLista extends Component {
    constructor(props) {
        super(props);
        this.getListOfertas();
    }

    getListOfertas() {
        this.props.getListOfertas(null, 0);
    }

    render() {
        const {list_ofertas} = this.props;
        // console.log(list_ofertas)
        return (
            <Main>
                <div className="container page">
                    <div className="row">
                        <div className="col-12">
                            <TitleSection title="Nossas" subtitle="Ofertas" />
                        </div>

                        {list_ofertas.size === 0 ? (
                            <div className="loading p-5">
                                <Loader type="Oval" color="#DB0000" height={100} width={100} />
                                {/* <h2>Em breve super ofertas</h2> */}
                            </div>
                        ) : (
                            list_ofertas.map((item, index) => {
                                return item ? <OfertasBox item={item} key={index} /> : null;
                            })
                        )}
                    </div>
                </div>
            </Main>
        );
    }
}

const mapStateToProps = (state) => ({
    list_ofertas: state.ofertas.get("list_ofertas"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListOfertas}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OfertasLista);
