import React, {Component} from "react";
import Header from "./elements/Header";
import Footer from "./elements/Footer";

import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {isDarkMode} from "../actions/DarkModeAction";

import {getListSettings} from "../actions/SettingsAction";

class Main extends Component {
    constructor(props) {
        super(props);
        this.getListSettings();
    }
    getListSettings() {
        this.props.getListSettings(null, 0);
    }
    render() {
        const {list_settings} = this.props;
        return (
            <React.Fragment>
                <Header settings={list_settings} />
                <main className={"main " + (isDarkMode() === "1" ? "active-dark-mode" : "")}>{this.props.children}</main>
                <Footer settings={list_settings} />
            </React.Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    list_settings: state.settings.get("list_settings"),
});

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getListSettings,
        },
        dispatch
    );

export default connect(mapStateToProps, mapDispatchToProps)(Main);
