import React, {Component} from "react";

export default class ShowroomBox extends Component {
    image(item) {
        if (item.get("capa")) {
            // return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa");
            return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa")

        } else {
            return item.getIn(["model", "image"]);
        }
    }
    render() {
        const {item} = this.props;
        return (
            <div className="item col-12 col-sm-6 col-lg-4 col-xl-3">
                <a href={"/showroom/" + item.getIn(["model", "slug"])}>
                    <div className="car-item" style={{backgroundImage: "url(" + this.image(item) + ")"}}>
                        <div className="bg">
                            <h1>{item.getIn(["model", "name"])}</h1>
                            <p>
                                Conheça melhor <i className="fa fa-caret-right" />
                            </p>
                        </div>
                    </div>
                </a>
            </div>
        );
    }
}
