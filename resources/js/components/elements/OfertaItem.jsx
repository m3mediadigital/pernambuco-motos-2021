import React, {Component} from "react";
import TitleSection from "./TitleSection";
import Payments from "../helpers/Payments";
import FormOrcamento from "../elements/FormOrcarmento";
import Gallery from "../elements/Gallery";

export default class OfertaItem extends Component {
    price(value) {
        let price_full = value.substr(0, value.length - 3);
        let price_1 = price_full.substr(0, price_full.length - 3);
        let price_2 = price_full.slice(3);
        return price_1 + "." + price_2 + "<sup>," + value.slice(-2);
    }
    render() {
        const {item_oferta} = this.props;

        return (
            <div className="row">
                <div className="col-12 pb-5">
                    <TitleSection title="Nossas" subtitle="Ofertas" />
                </div>
                <div className="d-lg-none col-12 position-relative">
                    <div className="tag">{item_oferta.getIn(["model", "year"])}</div>
                    <h1 className="title">{item_oferta.getIn(["model", "brand", "name"]) + " " + item_oferta.getIn(["model", "complete_name"])}</h1>
                </div>
                <div className="col-12 col-lg-6">
                    <img src={item_oferta.getIn(["color", "image"]) ? item_oferta.getIn(["color", "image"]) : item_oferta.getIn(["model", "image"])} alt={item_oferta.getIn(["model", "complete_name"])} className="img-fluid" />
                </div>
                <div className="col-12 col-lg-6 position-relative">
                    <div className="tag d-none d-lg-block">{item_oferta.getIn(["model", "year"])}</div>
                    <h1 className="title d-none d-lg-block">{item_oferta.getIn(["model", "brand", "name"]) + " " + item_oferta.getIn(["model", "complete_name"])}</h1>
                    <p>{item_oferta.getIn(["version", "complete_name"])}</p>
                    <Payments car={item_oferta} />
                    <p className="pt-5 pb-3">{item_oferta.get("description") ? item_oferta.get("description") : null}</p>
                    <a href="#euquero" className="more">Tenho interesse</a>
                </div>
                <div className="page seminovo col-12">
                    <Gallery item={item_oferta.get("model")} />
                </div>
                <div id="euquero">
                    <FormOrcamento id="ola" form="ofertas" carro={item_oferta.getIn(["model", "complete_name"])} value={item_oferta.get("id")} slug={item_oferta.get("slug")} />
                </div>
                
            </div>
        );
    }
}
