import React, { Component } from "react";
import CheeseburgerMenu from "cheeseburger-menu";
import logo from "../../assets/images/logo.png";
import logoWhite from "../../assets/images/logo-white.svg";

import { isDarkMode, deactiveDarkMode, activeDarkMode } from "../../actions/DarkModeAction";

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menuOpen: false,
            darkMode: isDarkMode(),
            fontSize: 0,
            visible: true,
        };
    }

    componentDidMount() {}

    // Remove the event listener when the component is unmount.
    componentWillUnmount() {}

    openMenu() {
        this.setState({ menuOpen: true });
    }

    closeMenu() {
        this.setState({ menuOpen: false });
    }

    whatsappLink(number) {
        const numberWhats = number.replace(/[^0-9]/g, "");

        return "https://api.whatsapp.com/send?phone=55" + numberWhats + "&text=Oi!";
    }

    render() {
        return (
            <header className={this.state.darkMode === "1" ? "active-dark-mode" : ""}>
                <div className="acessiblity d-none d-md-block ">
                    <div className="left-side">
                        <span>
                            Acessibilidade:
                            <button className="light-mode" onClick={() => deactiveDarkMode()} />
                            <button className="dark-mode" onClick={() => activeDarkMode()} />
                        </span>
                        {/* |
                        <span>
                            <button className="font-increase">A+</button>
                            <button className="font-decrease">A-</button>
                        </span> */}
                    </div>
                    <div className="right-side">
                        <a href="/quem-somos">Quem Somos</a>
                        <a href="/analise-credito">Análise de crédito</a>
                        <a href="/contato">Contato</a>
                    </div>
                </div>
                <div className="container-fluid">
                    <nav className="row d-none d-lg-flex">
                        <div className="col-6">
                            <a href="/" className="logo">
                                <img src={this.state.darkMode === 1 ? logo : logo} alt="logo" />
                            </a>
                        </div>
                        <div className="col-6 justify-content-end">
                            <ul className="menu">
                                <li>
                                    <a href="/ofertas" className="right-ball">
                                        Ofertas
                                    </a>
                                </li>
                                 <li>
                                    <a href="/servicos" className="">
                                        Serviços
                                    </a>
                                </li>
                                 <li>
                                    <a href="/acessorios" className="">
                                        Acessórios
                                    </a>
                                </li>
                                <li>
                                    <a href="/consorcio">Consórcio</a>
                                </li>
                                {this.props.settings.get("social_whatsapp") && (
                                    <li>
                                        <a className="zap" href={this.whatsappLink(this.props.settings.get("social_whatsapp"))} target="_blank" rel="noopener noreferrer">
                                            <i className="fa fa-whatsapp mr-2"></i>
                                            <span>{this.props.settings.get("social_whatsapp")}</span>
                                        </a>
                                    </li>
                                )}
                                <li>
                                    <div className="col-3 offset-3 text-center">
                                        <button className="menu-mobile-open" onClick={this.openMenu.bind(this)}>
                                            <i className="fa fa-bars"></i>
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div className="row d-flex d-lg-none">
                        <div className="col-6 d-flex flex-column justify-content-center">
                            <a href="/" className="logo">
                                <img src={this.state.darkMode === 1 ? logoWhite : logo} alt="logo" className="img-fluid" />
                            </a>
                        </div>
                        <div className="col-3 offset-3 text-center">
                            <button className="menu-mobile-open" onClick={this.openMenu.bind(this)}>
                                <i className="fa fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <CheeseburgerMenu isOpen={this.state.menuOpen} right={true} overlayClassName="botao-menu" closeCallback={this.closeMenu.bind(this)} {...this.props.menuProps}>
                    <div className={"menu-mobile " + (this.state.darkMode === 1 ? "active-dark-mode" : "")}>
                        <div className="head-menu d-flex justify-content-end">
                            {/* <a href="/" className="logo">
                                <img src={this.state.darkMode === 1 ? logoWhite : logo} alt="logo" />
                            </a> */}
                            <button className="fechar flex-grow-0" onClick={this.closeMenu.bind(this)}>
                                <i className="fa fa-times" aria-hidden="true" />
                            </button>
                        </div>

                        <ul className="navbar-nav">
                            <li>
                                <a href="/">Home</a>
                            </li>
                            <li>
                                <a href="/ofertas" className="right-ball">
                                    Ofertas
                                </a>
                            </li>
                            <li>
                                <a href="/acessorios" className="">
                                    Acessorios
                                </a>
                            </li>
                            <li>
                                <a href="/consorcio" className="">
                                    Consórcios
                                </a>
                            </li>
                            <li>
                                <a href="/showrooms" className="">
                                    Showroom
                                </a>
                            </li>
                            <li>
                                <a href="https://www.pernambucomotosatacado.com.br/" target="_blank" rel="noopener noreferrer">
                                    Peças
                                </a>
                            </li>
                            <li>
                                <a href="/contato">Contato</a>
                            </li>
                        </ul>
                        {this.props.settings.get("social_whatsapp") && (
                            <a className="zap" href={this.whatsappLink(this.props.settings.get("social_whatsapp"))} target="_blank" rel="noopener noreferrer">
                                <i className="fa fa-whatsapp mr-2"></i>
                            </a>
                        )}
                    </div>
                </CheeseburgerMenu>
            </header>
        );
    }
}
