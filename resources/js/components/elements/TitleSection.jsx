import React, {Component} from "react";

export default class TitleSection extends Component {
    render() {
        const {title, subtitle, isHome} = this.props;
        return (
            <div className="col-12">
                <div className="section-title">
                    <h1 style={{fontSize: isHome ? "35px" : "30px"}}>{title}</h1>
                    <h2 className="text-capitalize" style={{fontSize: isHome ? "27px" : "18px"}}>
                        {subtitle}
                    </h2>
                </div>
            </div>
        );
    }
}
