import React, {Component} from "react";
import Payments from "../helpers/Payments";

export default class OfertasBox extends Component {
    price(value) {
        let formatter = new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "BRL",
        });
        let price = formatter.format(value).split(".");
        return price[0].replace(",", ".").replace("R$", "") + "<sup>," + price[1] + "</sup>";
    }

    Selo(value) {
         console.log(value.length)
        if(value.size > 0)
            return (
                // <img src={value.getIn(["selo","image"])} className="selo" alt={value.getIn(["model", "complete_name"])} />
                <p>{value}</p>
            )
        return false
    }
    render() {
        const {item} = this.props;
        return (
            <div className="col-12 col-sm-6 col-lg-4 mb-5 mt-3" key={item.get("slug")} id={item.get("slug")}>
                <div className="item-direct-sales">
                    <a href={"/oferta/" + item.get("slug")}>
                        <h1>{item.getIn(["model", "complete_name"])} </h1>
                        <p>{item.getIn(["version", "complete_name"])}</p>
                        <img src={item.getIn(["model", "image"]) ? item.getIn(["model", "image"]) : item.getIn(["version", "image"])} alt="" className="img-fluid" />

                        <Payments car={item} />
                        {item.getIn(["selo","image"]) && (<img src={item.getIn(["selo","image"])} className="selo" alt={item.getIn(["model", "complete_name"])} />)}
                        
                    </a>
                    <p className="more">Tenho Interesse</p>
                </div>
            </div>
        );
    }
}
