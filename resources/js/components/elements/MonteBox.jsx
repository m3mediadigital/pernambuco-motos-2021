import React, {Component} from 'react'

export default class MonteBox extends Component {
    render() {
        const {slug, title, subtitle, image, selected, onClick, classname} = this.props;
        return (
            <div className={"col-12 col-sm-6 col-lg-3 mb-5 mt-3" + classname} key={slug} id={slug} onClick={onClick}>
                <div className={"border-item-moto " + (selected ? 'active' : '')}>
                    <div className={"item-monte-seu " + (selected ? 'active' : '')}>
                        <h1>{title}</h1>
                        <p>{subtitle}</p>
                        <img src={image} alt={title} className="img-fluid"/>
                    </div>
                </div>
            </div>
        )
    }
}