import React, {Component} from 'react'

export default class TitleSection extends Component {

    render() {
        const {title, subtitle, text} = this.props;
        return (
            <div className="col-12">
                <div className="title-page">
                    <h1>{title} <strong>{subtitle}</strong></h1>
                    <p>{text}</p>
                </div>
            </div>
        )
    }
}
