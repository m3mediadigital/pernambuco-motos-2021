import React, { Component } from "react";

import m3 from "../../assets/images/m3.svg";
import { bindActionCreators } from "redux";
import { getListNovas } from "../../actions/NovasAction";
import connect from "react-redux/es/connect/connect";
import { isDarkMode } from "../../actions/DarkModeAction";
import Follow from "../sections/Follow";
import IconZap from '../../assets/images/icon-whatsapp.png'

class Footer extends Component {
    constructor(props) {
        super(props);
        this.getListNovas();
    }

    getListNovas() {
        this.props.getListNovas(null, 0);
    }

    whatsappLink(number) {
        const numberWhats = number.replace(/[^0-9]/g, "");

        return "https://api.whatsapp.com/send?phone=55" + numberWhats + "&text=Oi!";
    }

    render() {
        const { list_novas } = this.props;
        const { settings } = this.props;
        return (
            <div>
                <div className={"container-fluid pt-2 " + (isDarkMode() === "1" ? "active-dark-mode" : "")}>
                    <Follow item={settings} />
                </div>
                <div id="div-footer">
                    <footer className={isDarkMode() === "1" ? "active-dark-mode" : ""}>
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-4 d-none d-lg-block">
                                    <strong>Guanabara Motos</strong>
                                    <ul className="menu-footer">
                                        <li>
                                            <a href="/ofertas">Ofertas</a>
                                        </li>
                                        <li>
                                            <a href="/seminovos">Seminovos</a>
                                        </li>
                                        <li>
                                            {/* <a href="/acessorios">Peças</a> */}
                                            <a href="https://www.pernambucomotosatacado.com.br/" target="_blank" rel="noopener noreferrer">
                                                Peças
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/acessorios">Acessórios</a>
                                        </li>
                                        <li>
                                            <a href="/pos-vendas">Serviços</a>
                                        </li>
                                        <li>
                                            <a href="/consorcio">Consórcio</a>
                                        </li>
                                        <li>
                                            <a href="/venda-direta">Vendas Diretas</a>
                                        </li>
                                        <li>
                                            <a href="/analise-credito">Análise de crédito</a>
                                        </li>
                                        <li>
                                            <a href="/contato">Contato</a>
                                        </li>
                                        <li>
                                            <a href="/quem-somos">Quem somos</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="col-lg-4  d-none d-lg-block">
                                    <strong>Showroom</strong>
                                    <ul className="menu-footer">
                                        {list_novas.size === 0 ? (
                                            <div className="item" key="1"></div>
                                        ) : (
                                            list_novas.slice(0, 10).map((item) => {
                                                return item ? (
                                                    <li key={item.get("id")}>
                                                        <a href={"/showroom/" + item.get("slug")}>{item.getIn(["model", "name"])}</a>
                                                    </li>
                                                ) : null;
                                            })
                                        )}
                                    </ul> 
                                </div>
                                <div className="col-lg-4 col-12">
                                    <strong className="text-center text-lg-left">Nossa localização</strong>
                                    <p className="address text-center text-lg-left">
                                        Estrada do Galeão, 1891 - Jardim Guanabara, Rio de Janeiro - Brasil <br />   
                                        {settings.get("social_whatsapp") && (
                                            <a href={this.whatsappLink(settings.get("social_whatsapp"))} target="_blank" rel="noopener noreferrer">
                                                <i className="fa fa-phone" /> {settings.get("social_whatsapp")}
                                            </a>
                                        )}
                                    </p>
                                </div>
                                {/* <hr className="d-none d-lg-block" />
                                <div className="col-lg-12 d-none d-lg-block">
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet delectus, dicta non ducimus asperiores reiciendis iusto modi nostrum harum optio similique, ex numquam veritatis quod culpa, ullam expedita dolorem! Earum.</p>
                                </div> */}
                            </div>
                        </div>
                        <div className="subfooter">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-12 col-12 text-lg-right text-center">
                                        <div className="download-app link pt-3">
                                            <div className="link-content">
                                                <ul>
                                                    <li className="d-flex align-items-center">Uma empresa do Guanabara Motos</li>
                                                    <li>
                                                        <a href="/" className="interesse">
                                                            Ver sites do grupo
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a href="https://novam3.com.br" className="footer-logo" target="_blank" rel="noopener noreferrer">
                                                <img src={m3} alt="Nova M3" />
                                            </a>
                                            <div>
                                                {settings.get("social_whatsapp") && (
                                                    <a href={this.whatsappLink(settings.get("social_whatsapp"))} target="_blank" rel="noopener noreferrer" title="Entre em contato!">
                                                        <img className="icon-zap" src={IconZap} alt="Whatsapp" />
                                                    </a>
                                                )}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    list_novas: state.novas.get("list_novas"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({ getListNovas }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Footer);

// export default Footer
