import React, {Component} from "react";
import TitleSection from "./TitleSection";
import FormOrcamento from "./FormOrcarmento";

export default class DirectSalesItem extends Component {
    price(value) {
        let price_full = value.substr(0, value.length - 3);
        let price_1 = price_full.substr(0, price_full.length - 3);
        let price_2 = price_full.slice(3);
        return price_1 + "." + price_2 + "<sup>," + value.slice(-2);
    }
    render() {
        const {item} = this.props;
        const tag_color = {
            pcd: {
                color: "#154DA1",
                label: "PCD",
            },
            micro_empresario: {
                color: "#AB1A1A",
                label: "Micro Empresário",
            },
            produtor_rural: {
                color: "#6E451C",
                label: "Produtor Rural",
            },
            produtor_rural_mda: {
                color: "#6E451C",
                label: "Produtor Rural MDA",
            },

            taxista: {
                color: "#154DA1",
                label: "Taxista",
            },
            frotista: {
                color: "#AB1A1A",
                label: "Frotista",
            },
            locadora: {
                color: "#6E451C",
                label: "Produtor RuraLocadoral",
            },

            auto_escola: {
                color: "#154DA1",
                label: "Auto Escola",
            },
            motorista_aplicativo: {
                color: "#AB1A1A",
                label: "Motorista de Aplicativo",
            },
            escolar_autônomo: {
                color: "#6E451C",
                label: "Escolar e Autônomo",
            },
            corpo_diplomatico: {
                color: "#154DA1",
                label: "Corpo Diplomático",
            },
            grandes_frotistas: {
                color: "#154DA1",
                label: "Grandes Frotistas",
            },
            governo: {
                color: "#154DA1",
                label: "Governo",
            },

            deficiente_fisico: {
                color: "#154DA1",
                label: "Deficiênte Fisíco",
            },
            meepp: {
                color: "#154DA1",
                label: "ME / EPP",
            },
        };

        return (
            <div className="row">
                <div className="col-12 pb-5">
                    <TitleSection title="Vendas diretas" subtitle={tag_color[item.get("corporate_sales_category") === "me_/_epp" ? "meepp" : item.get("corporate_sales_category")].label} />
                </div>
                <div className="col-12 col-lg-6">
                    <img src={item.getIn(["color", "image"]) ? item.getIn(["color", "image"]) : item.getIn(["model", "image"])} alt={item.getIn(["model", "complete_name"])} className="img-fluid" />
                </div>
                <div className="col-12 col-lg-6 position-relative">
                    <div className="tag">{item.getIn(["model", "year"])}</div>
                    <h1 className="title">{item.getIn(["model", "brand", "name"]) + " " + item.getIn(["model", "complete_name"])}</h1>
                    <p> {item.getIn(["version", "name"])}</p>

                    {item.get("value_from") ? (
                        <div>
                            <span className="description">Por apenas R$</span> <br />
                            <span className="price" dangerouslySetInnerHTML={{__html: this.price(item.get("value_from"), {sanitize: true})}} />
                        </div>
                    ) : (
                        <span className="price">Consulte-nos</span>
                    )}

                    {/* <div className="more">Tenho interesse</div> */}
                </div>
                <FormOrcamento form="vendas_diretas" carro={item.getIn(["model", "complete_name"])} idDirect={item.get("id")} slug={item.get("slug")} />
            </div>
        );
    }
}
