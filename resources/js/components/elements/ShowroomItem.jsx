/* eslint-disable jsx-a11y/aria-proptypes */
import React, {Component} from "react";
import TitleSection from "./TitleSection";
import FormOrcarmento from "../elements/FormOrcarmento";
import Slider from "react-slick";

export default class ShowroomItem extends Component {
    image(item) {
        if (item.get("capa")) {
            // return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa");
            return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa")

        } else {
            return item.getIn(["model", "image"]);
        }
    }
    price(value) {
        let price_full = value.substr(0, value.length - 3);
        let price_1 = price_full.substr(0, price_full.length - 3);
        let price_2 = price_full.slice(3);
        return price_1 + "." + price_2 + "<sup>," + value.slice(-2);
    }
    render() {
        const {item_nova} = this.props;
        return (
            <>
                <img src={item_nova.get("dream_banner")} alt={item_nova.getIn(["model", "complete_name"])} className="img-fluid w-100" />
                <div className="page seminovo">
                    <div className="container">
                        <div className="pt-5 pb-5">
                            <Accordion item={item_nova.get("model")} />
                            {/* <div className="row pt-5 pb-5">
                                <div className="col-12">
                                    <strong>Confira todas as versões</strong>
                                </div>
                                {item_nova != null && item_nova.size > 0
                                    ? item_nova.get("versoes").map((versao, index) => (
                                          <div className="col-3" key={index}>
                                              <img src={versao.get("image")} alt="" className="img-fluid" />
                                              <p>{versao.get("complete_name")}</p>
                                          </div>
                                      ))
                                    : null}
                            </div> */}
                            <Galleria item={item_nova.get("model")} />
                            <FormOrcarmento form="novos" carro={item_nova.getIn(["model", "complete_name"])} />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

const Galleria = ({item}) => {
    const image = item.get("gallery");
    return (
        <div className="gallery pt-5">
            <TitleSection title="Galeria" subtitle="" />
            <Slider {...params}>
                {image.size === 0
                    ? null
                    : image.map((item, index) => {
                          return (
                              <div className="item" key={index}>
                                  <img src={item.get("path")} className="img-fluid w-100" alt="Guanabara" />
                              </div>
                          );
                      })}
            </Slider>
        </div>
    );
};

const params = {
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1000,
    nav: true,
};

const Accordion = ({item}) => {
    const sobre = item.get("highlight");
    return (
        <div className="accordion pb-5">
            <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                {sobre.size === 0
                    ? null
                    : sobre.map((item, index) => {
                          return (
                              <li className="nav-item" key={index}>
                                  <a className={`nav-link ${item.get("position") === 1 ? "active right-angle-arrow" : ""}`} id={"pills-home-" + item.get("id") + item.get("models_id")} data-toggle="pill" href={"#pills-home" + item.get("id") + item.get("models_id")} role="tab" aria-controls={"pills-home" + item.get("id") + item.get("models_id")} aria-selected={`${item.position === 1 ? true : false}`}>
                                      {item.get("name")}
                                  </a>
                              </li>
                          );
                      })}
                <li className="nav-item interesse">
                    <a className="nav-link" href="#tenho-interece">
                        tenho interesse
                    </a>
                </li>
            </ul>
            <div className="tab-content pt-2" id="pills-tabContent">
                {sobre.size === 0
                    ? null
                    : sobre.map((item, index) => {
                            return (
                                <div key={index} className={`tab-pane fade ${item.get("position") === 1 ? "show active" : ""}`} id={"pills-home" + item.get("id") + item.get("models_id")} role="tabpanel" aria-labelledby={"pills-home-" + item.get("id") + item.get("models_id")}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-7">
                                            <div className="background" style={{backgroundImage: `linear-gradient(rgba(0,0,0,.2), rgba(0,0,0,0.5)),url(${item.get("image")})`}}>
                                                <p>
                                                    {item.get("subtitle")} <hr />
                                                </p>
                                            </div>
                                            <div className="bg"></div>
                                        </div>
                                        <div className="col-sm-12 col-md-5 d-flex align-items-center">
                                            <div dangerouslySetInnerHTML={{__html: item.get("content")}} />
                                        </div>
                                    </div>
                                </div>
                            );
                      })}
            </div>
        </div>
    );
};
