import React, {Component} from "react";
import Slider from "react-slick";

import {bindActionCreators} from "redux";
import {getListSlides} from "../../actions/SlidesAction";
import connect from "react-redux/es/connect/connect";
// import { GrPrevious } from 'react-icons/gr';
import Next from '../../assets/images/arrow-right-2.png'
import Prev from '../../assets/images/arrow-left.png'

function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
      <div>
          <div
            className={className}
            style={{ ...style, display: "block",  visibility: "" }}
            onClick={onClick}
          >
              {/* <GrNext size="35px"/> */}
              <img className="" src={Next} alt="Next" style={{height: "50px", width: "40", position: "relative", right: "20px"}}/>
          </div>
      </div>
  );
}
function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
      <div>
          <div
            className={className}
            style={{ ...style, display: "block", visibility: "" }}
            onClick={onClick}
          >
            {/* <GrPrevious size="35px" color="white"/> */}
              <img className="" src={Prev} alt="Previous" style={{height: "50px"}}/>

            </div>
      </div>
    );
}

const params = {
    arrows: true,
    lazyLoad: true,
    autoplay: true,
    autoplaySpeed: 10000, //10seg
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 1500,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />
};
class Banner extends Component {
    constructor(props) {
        super(props);
        this.getListSlides();
    }

    getListSlides() {
        this.props.getListSlides(null, 0);
    }

    render() {
        const {list_slides} = this.props;
        return (
            <section className="banners-home mb-5">
                <Slider {...params}>
                    {list_slides.size === 0 ? (
                        <div></div>
                    ) : (
                        list_slides.map((item) => {
                            return item ? (
                                <div className="item" key={"slide_" + item.get("id")}>
                                    <a href={item.get("link")}>
                                        <img src={item.get("image")} className="img-fluid" alt="Guanabara Motos" />
                                        {/* <div className="banner" style={{backgroundImage: "url(" + item.get("image") + ")"}}></div> */}
                                    </a>
                                </div>
                            ) : (
                                <div></div>
                            );
                        })
                    )}
                </Slider>
            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    list_slides: state.slides.get("list_slides"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListSlides}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Banner);
