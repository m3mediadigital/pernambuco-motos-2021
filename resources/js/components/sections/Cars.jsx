import React, {Component} from "react";
import Slider from "react-slick";
import "../../../node_modules/slick-carousel/slick/slick.scss";
import "../../../node_modules/slick-carousel/slick/slick-theme.scss";
import {getListNovas} from "../../actions/NovasAction";
import TitleSection from "../elements/TitleSection";
import {bindActionCreators} from "redux";
import connect from "react-redux/es/connect/connect";

const params = {
    autoplay: true,
    autoplaySpeed: 5000,
    cssEase: "linear",
    dots: false,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    speed: 1000,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: false,
            },
        },
        {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                initialSlide: 2,
            },
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            },
        },
    ],
};

class Cars extends Component {
    constructor(props) {
        super(props);
        this.getListNovas();
    }

    getListNovas() {
        this.props.getListNovas(null, 0);
    }
    image(item) {
        if (item.get("capa")) {
            // return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa");
            return "https://descomplicar.s3-sa-east-1.amazonaws.com/upload/" + item.get("capa")
        } else {
            return item.getIn(["model", "image"]);
        }
    }
    render() {
        const {list_novas} = this.props;
        return (
            <section className="cars-home mb-5">
                <div className="row">
                    <TitleSection title="Quem conhece" subtitle="tira o chapeu" isHome={true} />
                </div>
                <Slider {...params}>
                    {list_novas.size === 0 ? (
                        <div className="item" key="1"></div>
                    ) : (
                        list_novas.map((item, index) => {
                            return item ? (
                                <div className="item" key={index}>
                                    <a href={"/showroom/" + item.getIn(["model", "slug"])}>
                                        <div className="car-item" style={{backgroundImage: "url(" + this.image(item) + ")"}}>
                                            <div className="bg">
                                                <h1>{item.getIn(["model", "name"])}</h1>
                                                <p>
                                                    Conheça melhor <i className="fa fa-caret-right" />
                                                </p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            ) : null;
                        })
                    )}
                </Slider>
            </section>
        );
    }
}

const mapStateToProps = (state) => ({
    list_novas: state.novas.get("list_novas"),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({getListNovas}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cars);
