import {fromJS} from 'immutable'

const PLURAL = 'ACESSORIOS'
const SINGULAR = 'ACESSORIO'

const INITIAL_STATE = fromJS({
  list_acessorios: [],
  meta: [],
  item_acessorio: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_acessorios', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_acessorio', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_acessorio', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_acessorios', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_acessorio', fromJS([]))
    default:
      return state
  }
}