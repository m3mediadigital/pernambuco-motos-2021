import {fromJS} from 'immutable'

const PLURAL = 'INSTITUCIONAL'
// const SINGULAR = 'INSTITUCIONAL'

const INITIAL_STATE = fromJS({
  list_institucional: []
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_institucional', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_institucional', fromJS([]))
    default:
      return state
  }
}