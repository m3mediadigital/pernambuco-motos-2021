import {fromJS} from 'immutable'

const PLURAL = 'SEMINOVOS'
const SINGULAR = 'SEMINOVO'

const INITIAL_STATE = fromJS({
  list_seminovos: [],
  meta: [],
  item_seminovo: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_seminovos', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_seminovo', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_seminovo', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_seminovos', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_seminovo', fromJS([]))
    default:
      return state
  }
}