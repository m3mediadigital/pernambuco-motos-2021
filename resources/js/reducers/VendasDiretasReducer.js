import {fromJS} from 'immutable'

const PLURAL = 'VENDAS_DIRETAS'
const SINGULAR = 'VENDA_DIRETA'

const INITIAL_STATE = fromJS({
  list_vendas_diretas: [],
  meta: [],
  item_venda_direta: null
})

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case `SET_${PLURAL}`:
      return state.set('list_vendas_diretas', fromJS(action.payload))
    case `SET_${PLURAL}_META`:
      return state.set('meta', fromJS(action.payload))
    case `SET_${SINGULAR}`:
      return state.set('item_venda_direta', fromJS(action.payload))
    case `UNSET_${SINGULAR}`:
      return state.set('item_venda_direta', null)

    case `UNSET_${PLURAL}_INFO`:
      return state
        .set('list_vendas_diretas', fromJS([]))
        .set('meta', fromJS([]))
        .set('item_venda_direta', fromJS([]))
    default:
      return state
  }
}