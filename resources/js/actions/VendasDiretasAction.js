import SimpleCrud from "../services/simpleCrud";

const PLURAL = "VENDAS_DIRETAS";
const SINGULAR = "VENDA_DIRETA";
const LIST_URL = "/vendas-diretas";
const COMPANY = "30";
const METHOD_LIST = "getVendasDiretas";
const METHOD_GET = "getVendaDireta";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getVendaDireta = (slug) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getVendasDiretas = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
