import SimpleCrud from "../services/simpleCrud";

const PLURAL = "CONTATOS";
const SINGULAR = "CONTATO";
const COMPANY = "30";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const sendContato = (method, request) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.add(method, request, dispatch, SINGULAR);
    };
};
