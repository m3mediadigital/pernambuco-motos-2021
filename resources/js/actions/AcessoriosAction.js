import SimpleCrud from "../services/simpleCrud";

const PLURAL = "ACESSORIOS";
const SINGULAR = "ACESSORIO";
const LIST_URL = "/acessorios";
const COMPANY = "30";
const METHOD_LIST = "getAcessorios";
const METHOD_GET = "getAcessorio";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getAcessorio = (slug) => {
    return (dispatch) => {
        dispatch(unset());
        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getListAcessorios = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};
