import SimpleCrud from "../services/simpleCrud";

const PLURAL = "NOVAS";
const SINGULAR = "NOVA";
const LIST_URL = "/novas";
const METHOD_LIST = "getNovas";
const METHOD_GET = "getNova";
const METHOD_LIST_INDEX = "getNovasIndex";

export const unset = () => {
    return {
        type: `UNSET_${PLURAL}_INFO`
    };
};

export const getNova = (slug) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.get(METHOD_GET, slug, dispatch, SINGULAR, LIST_URL);
    };
};

export const getListNovas = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST, dispatch, PLURAL);
    };
};


export const getListNovasIndex = (per_page = 4, page) => {
    return (dispatch) => {
        dispatch(unset());

        SimpleCrud.list(per_page, page, METHOD_LIST_INDEX, dispatch, PLURAL);
    };
};
