import React from "react";

import Banner from "../components/sections/Banner";
// import Cars from "../components/sections/Cars";
import Main from "../components/Main";
// import Want from "../components/sections/Want";

import banner from "../assets/images/novo-banner-index.png";
import OfertasListIndex from "../components/elements/OfertasListIndex";
import PosVendaBoxIndex from "../components/elements/PosVendaBoxIndex";
// import IconZap from "../components/elements/IconZap";
// import IconZap from "../assets/images/icon-whatsapp.png";

export default (props) => (
    <Main>
        <Banner />
        {/* <IconZap /> */}
        <div className="container">
            <OfertasListIndex />
            {/* <Cars /> */}
            {/* <Want /> */}
        </div>
        <div className="container-fluid">
            <div className="row mt-5">
                <div className="col-12 col-lg-10 offset-lg-1">
                    <a href="/contato">
                        <div className="banner-footer" style={{backgroundImage: "url(" + banner + ")"}}></div>
                    </a>
                </div>
            </div>
        </div>
        <div className="container">
            <PosVendaBoxIndex />
        </div>
    </Main>
);
