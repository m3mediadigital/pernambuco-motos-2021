<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="description" content="Guanabara Moto - Concessionária Honda"/>
    <meta name="keywords" content=" cg 160, nxr 160 bros, pop 110i, biz 110i, biz 125, cg cargo, cg 125, fan, cg 160 start, titan, pcx, twister, bros, xre, pernambuco, concessionária honda, honda"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Guanabara Moto - Concessionária Honda"/>
    <meta property="og:description" content="Guanabara Moto - Concessionária Honda"/>
    <meta property="og:url" content="www.guanabaramotos.com.br"/>
    <meta property="og:site_name" content="Guanabara Moto - Concessionária Honda"/>
    <meta property="og:image" content=""/>
    <meta property="og:locale" content="pt_BR"/>

    <title>Guanabara Motos - Concessionária Honda</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">   
</head>
<body>
    <div id="root"></div>
    <script type="text/javascript" src="{{ asset('js/index.js') }}"></script>
</body>
</html>