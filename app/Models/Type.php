<?php

namespace App\Models;

class Type extends AppDescomplicarModel
{
	protected $table = 'type';
	
	public function models()
	{
		return $this->hasMany('App\Models\Model', 'type_id', 'id');
	}
	
	public function bodies()
	{
		return $this->hasMany('App\Models\Body', 'type_id', 'id');
	}
	
	public function ccs()
	{
		return $this->hasMany('App\Models\Cc', 'type_id', 'id');
	}
	
	public function motorizations()
	{
		return $this->hasMany('App\Models\Motorization', 'type_id', 'id');
	}
	
	public function transmissions()
	{
		return $this->hasMany('App\Models\Transmission', 'type_id', 'id');
	}
}
