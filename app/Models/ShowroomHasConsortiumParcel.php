<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ShowroomHasConsortiumParcel extends AppDescomplicarModel
{
    protected $table = 'showroom_has_consortium_parcels';
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
	
    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}

}
