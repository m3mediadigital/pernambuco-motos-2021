<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Brand extends AppDescomplicarModel
{
	protected $table = 'brands';

	public static function customAll()
	{
		return self::query()
            ->select('name')
           	->groupBy(['name'])
           	->orderBy('name', 'asc')
           	->get();
	}

	public static function allBrands()
	{
		return self::query()
			->select([
				'*',
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
			])
           	->orderBy('name', 'asc')
           	->get();
	}
}
