<?php

namespace App\Models;

class FinancingTable extends AppDescomplicarModel
{
	protected $table = 'financing_table_parcels';
	
    public function financing_table_parcels()
	{
		return $this->hasMany('App\Models\FinancingTableParcel', 'financing_table_id', 'id');
	}

    public function showroom()
	{
		return $this->hasMany('App\Models\Showroom', 'financing_table_id', 'id');
	}

}
