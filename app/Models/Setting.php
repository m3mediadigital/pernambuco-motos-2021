<?php

namespace App\Models;

class Setting extends AppDescomplicarModel
{
	protected $table = 'settings';
	
    public static function customFetchAll() 
    {
        $dados = self::where([
        		['companies_id', self::$COMPANY_ID]
        	])
        	->select('chave', 'valor')
        	->get();

        $dadosPlucked = $dados->pluck('valor', 'chave');

        return $dadosPlucked->all();
    }
}
