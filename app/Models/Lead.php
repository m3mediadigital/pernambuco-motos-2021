<?php

namespace App\Models;

class Lead extends AppDescomplicarModel
{
	protected $table = 'leads';
	
    public function form()
	{
	    return $this->belongsTo('App\Models\Form', 'forms_id', 'id');
	}
	
	public function custom_answer()
	{
		// return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
		return $this->hasMany('App\Models\CustomAnswer', 'leads_id', 'id');
	}
}
