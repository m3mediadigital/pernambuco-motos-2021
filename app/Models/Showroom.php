<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Showroom extends AppDescomplicarModel
{
	const CARRO = 1;
	const MOTO = 2;
	const QUADRICICLO = 3;

	protected $table = 'showroom';
	// protected $dates = ['finish_at'];

    public function color()
	{
	    return $this->belongsTo('App\Models\Color', 'colors_id', 'id');
	}

    public function financing_table()
	{
	    return $this->belongsTo('App\Models\FinancingTable', 'financing_table_id', 'id');
	}

    public function model()
	{
	    return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}

    public function version()
	{
	    return $this->belongsTo('App\Models\Version', 'versions_id', 'id');
	}

    public function versoes()
	{
	    return $this->belongsToMany('App\Models\Version', 'showroom_has_versions', 'showroom_id', 'versions_id', 'order_by');
	}

    public function optionals()
	{
	    return $this->belongsToMany('App\Models\Optional', 'showroom_has_optionals', 'showroom_id', 'optionals_id');
	}

	public function coupons()
	{
		return $this->hasMany('App\Models\Coupon', 'showroom_id', 'id');
	}

	public function custom_answers()
	{
		return $this->hasMany('App\Models\CustomAnswer', 'showroom_id', 'id');
	}

	public function showroom_visits()
	{
		return $this->hasMany('App\Models\ShowroomVisit', 'showroom_id', 'id');
	}

    public function consortium_parcels()
    {
        return $this->belongsToMany('App\Models\ConsortiumParcel', 'showroom_has_consortium_parcels', 'showroom_id', 'consortium_parcels_id');
    }

    private static function containShowroomOferta(&$query, $custom_answer = true)
    {
        $query->with([
            'model' => function($q) {
                $q->select( 
                	'id',
                    'name',
                    'complete_name',
                    'year',
                 //    'description',
                    'brands_id',
                    'cc_id',
                    'motorization_id',
                    'bodies_id',
                    'fuels_id',
                    // '*',
                	DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                )
                ->with([
                    'brand' => function($q) {
                        $q->select('id', 'name');
                    },
                    'gallery' => function($q) {
                        $q->select(
                            'models_id',
                            // 'type',
                            // 'youtube_id',
                            // 'file',
                            'path'
                        );
                    },
                    'cc' => function($q) {
                        $q->select('id', 'value');
                    },
                    'fuel' => function($q) {
                        $q->select('id', 'value');
                    },
                    'body' => function($q) {
                        $q->select('id', 'value');
                    },
                    'motorization' => function($q) {
                        $q->select('id', 'value');
                    },
                    'transmission' => function($q) {
                        $q->select('id', 'value');
                    },
                    'colors' => function($q) {
                        return $q->select('models_id', 'hexadecimal', DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as color') );
                    }
                ]);
            },
            // 'version' => function($q) {
            //     $q->select(
            //         'id',
            //         DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", large_image) as image'),
            //         'name',
            //         'complete_name',
            //         'slug'
            //     );
            // },
            'optionals' => function($q) {
                $q->select(
                    'id',
                    'value',
                    'icon',
                    'svg'
                );
            },
            // 'color' => function($q) {
            //     $q->select(
            //         'id',
            //         'color',
            //         DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
            //     );
            // }

        ]);
        // ->join('models', 'showroom.models_id', '=', 'models.id');

        if($custom_answer) {
        	$query->with([ 'custom_answers' => function($q) {
	                $q->with([ 'custom_question' => function($q) {
	                        $q->select([
	                            'custom_questions.id',
	                            'custom_questions.slug',
	                            'custom_questions.type'
	                        ]);
	                    }
	                ]);
	            }
	        ]);
        }

    }

    private static function basicContainShowroomOferta(&$query, $custom_answer = true)
    {
        $query->with([
            'model' => function($q) {
                $q->select( 
                    'id',
                    'slug',
                    'name',
                    'complete_name',
                    'brands_id',
                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                )
                ->with([
                    'brand' => function($q) {
                        $q->select('id', 'name');
                    },
                    // 'colors' => function($q) {
                    //     return $q->select('models_id', 'hexadecimal', DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as color') );
                    // }
                ]);
            },
            // 'version' => function($q) {
            //     $q->select(
            //         'id',
            //         DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", large_image) as image'),
            //         'name',
            //         'complete_name',
            //         'slug'
            //     );
            // },
            'color' => function($q) {
                $q->select(
                    'id',
                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                );
            }
        ]);

        if($custom_answer) {
            $query->with([ 'custom_answers' => function($q) {
                    $q->with([ 'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                }
            ]);
        }

    }

    private static function containShowroomListOferta(&$query, $custom_answer = true)
    {
        $query->with([
            'model' => function($q) {
                $q->select( 
                    'id',
                    'slug',
                    'name',
                    'complete_name',
                    'brands_id',
                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                )
                ->with([
                    'brand' => function($q) {
                        $q->select('id', 'name');
                    },
                    'colors' => function($q) {
                        return $q->select('models_id', 'hexadecimal', DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as color') );
                    }
                ]);
            }
        ]);

        if($custom_answer) {
            $query->with([ 'custom_answers' => function($q) {
                    $q->with([ 'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                }
            ]);
        }

    }

    private static function customPaginate(&$query, $order = [], $limit = null, $page = null, $idsOutside = null)
    {

        if($order){
        	if( is_array($order) && count($order) == 2 ){
	        	list($field, $ord) = $order;
	            $query->orderBy($field, $ord);
	        }else if( $order == 'RAND' ){
	        	$query->inRandomOrder();
	        }
        }

        if($idsOutside) {
            $query->whereNotIn('showroom.id', $idsOutside);
        }
        
        if($limit) {
            $query->limit($limit);
            
            if($page) {
                $query->offset( ($page*$limit) );
            }
        }

    }

    public static function customDream($type_vehicle = null, $order = null, $limit = null, $page = null, $idsOutside = null )
    {
        $query = self::customBuildQueryNovos();
        self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();

        $dreams = [];
        // foreach($dados as $item):
        //     ;
        // endforeach;

        foreach($dados as $item):
            if(isset($item->custom_answers[6])):    
                foreach($item->custom_answers as $value):
                    if($value->custom_options !== null):
                        if($value->custom_questions_id === 549):
                            $dreams[] = $item;
                            self::singleCustomKey($item);
                        endif;
                    endif;
                endforeach;
            endif;
        endforeach;

        // dd($dreams);
        
        return $dreams;
    }

    private static function customBuildQueryOfertas()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.payment_method',
                'showroom.parcel_amount',
                'showroom.parcel_value',
                'showroom.entry',
                'showroom.bonus',
                'showroom.value_from',
                'showroom.value_to',
                'showroom.finish_at',
                'showroom.type',
                'showroom.colors_id',
                'showroom.color',
                'showroom.brand_new_value',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.type',
                // DB::raw('(SELECT ((COUNT(DISTINCT showroom_visits.ip) + 1) * 3) FROM showroom_visits WHERE showroom_visits.showroom_id = showroom.id) as total_visualizacoes')
            ]);

        self::containShowroomOferta($query);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'oferta'],
                ['showroom.finish_at', '>=', date('Y-m-d')]
            ]);

        return $query;
    }

    private static function customBuildQueryListOfertas()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.payment_method',
                'showroom.parcel_amount',
                'showroom.parcel_value',
                'showroom.entry',
                'showroom.bonus',
                'showroom.value_from',
                'showroom.value_to',
                'showroom.finish_at',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.colors_id'
            ]);

        self::basicContainShowroomOferta($query);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'oferta'],
                ['showroom.finish_at', '>=', date('Y-m-d')]
            ]);

        return $query;
    }

    public static function customListAllOfertas()
    {
        $query = self::customBuildQueryListOfertas();        
        $ofertas = $query->get();
        self::singleCustomKeyWithNoSQLQuery($ofertas);

        return $ofertas;
    }

    public static function customOfertaBySlug($slug, $model_slug = false, $order = [])
    {
    	$query = self::customBuildQueryOfertas();

        if($model_slug) {
            $query->where('models.slug','=', $slug);
        }else {
            $query->where('showroom.slug', '=', $slug);
        }

        if($order){
            $query->order($order);
        }

        $oferta = $query->first();
        
        if( $oferta ){
        	self::singleCustomKey($oferta);
        }

        return $oferta;
    	
    }

    public static function customOfertaDiffSlug($slug, $model_slug = false, $limit = null)
    {
        $query = self::customBuildQueryOfertas();

        if($model_slug) {
            $query->where('models.slug','!=', $slug);
        }else {
            $query->where('showroom.slug', '!=', $slug);
        }

        if($limit){
            $query->limit($limit);
        }

        $ofertas = $query->inRandomOrder()->get();
        
        foreach($ofertas as $oferta) {
            if( $oferta ){
                self::singleCustomKey($oferta);
                $oferta->full_name = $oferta->model->complete_name.( $oferta->version ? ' '.$oferta->version->complete_name : '');
                
            }
        }

        return $ofertas;
        
    }

    private static function customBuildQueryNovos($containAllVersions = false)
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.brand_new_value',
                'showroom.slug',
                'showroom.color',
                'showroom.type'
            ])
    		->with([
                'versoes' => function($q) {
                    $q->select(
                        'versions.id',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", versions.large_image) as image'),
                        'versions.name',
                        'versions.complete_name',
                        'versions.slug'
                    )
                        ->with([
                            'colors' => function($q) {
                                $q->select(
                                    'versions_id',
                                    'hexadecimal',
                                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                                    'color'
                                );
                            }
                        ])
                        ->whereNotNull('large_image')
                        ->orderBy('showroom_has_versions.order_by','asc');
                },
                'model' => function($q) {
                    $q->select( 
                    	'models.id',
                        'models.slug',
                        'models.name',
                        'models.complete_name',
                        'models.brands_id',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                    )->with([
                        'brand' => function($q) {
                            $q->select('id', 'name');
                        },
                        'colors' => function($q) {
                            return $q->select('models_id', 'hexadecimal', DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as color') );
                        }
                    ]);
                },

                'custom_answers' => function($q) {
                    $q->with(['custom_options', 'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                },
            ]);
        
        $query->where([
                ['showroom.type', '=', 'novo'],
                ['showroom.active', '=', 1],
                ['showroom.companies_id', '=', self::$COMPANY_ID]
            ])->orderBy('showroom.slug', 'asc');

	    return $query;
    }

    private static function customBuildQueryNovosIndex($containAllVersions = false)
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.brand_new_value',
                'showroom.slug',
                'showroom.color',
                'showroom.type'
            ])
    		->with([
                'model' => function($q) {
                    $q->select( 
                    	'models.id',
                        'models.slug',
                        'models.name',
                        'models.complete_name',
                        'models.brands_id',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                    );
                },
            ]);
        
        $query->where([
                ['showroom.type', '=', 'novo'],
                ['showroom.active', '=', 1],
                ['showroom.companies_id', '=', self::$COMPANY_ID]
            ])->orderBy('showroom.slug', 'asc');

	    return $query;
    }

    private static function customBuildQueryNovo($containAllVersions = false)
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.brand_new_value',
                'showroom.slug',
                'showroom.color',
                'showroom.type'
            ])
            ->with([
                'model' => function($q) {
                    $q->select( 
                        'id',
                        'brands_id',
                        'name',
                        'complete_name',
                        'slug',
                        'year',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", manual) as manual')
                    )
                    ->with([
                        'brand' => function($q) {
                            $q->select('id', 'name');
                        },
                        'gallery' => function($q) {
                            $q->select(
                                'id',
                                'models_id',
                                'path'
                            );
                        },
                        'highlight' => function($q) {
                            $q->select(
                                '*',
                                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                            )
                            ->where('connection', '=', 'novo')
                            ->orderBy('position','asc');
                        },
                        // 'cc' => function($q) {
                        //     return $q->select('id', 'value');
                        // },
                        // 'fuel' => function($q) {
                        //     return $q->select('id', 'value');
                        // },
                        // 'body' => function($q) {
                        //     return $q->select('id', 'value');
                        // },
                        // 'motorization' => function($q) {
                        //     return $q->select('id', 'value');
                        // },
                        // 'transmission' => function($q) {
                        //     return $q->select('id', 'value');
                        // },
                        'colors' => function($q) {
                            return $q->select('models_id', 'hexadecimal', DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as color') );
                        }
                    ]);
                },
                'versoes',
                'custom_answers' => function($q) {
                    $q->with(['custom_options', 'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                },
            ]);

        if( $containAllVersions ){
            $query->with([
                'versoes' => function($q) {
                    $q->select(
                        'versions.id',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", versions.large_image) as image'),
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", versions.banner_image) as banner_image'),
                        'versions.name',
                        'versions.complete_name',
                        'versions.slug',
                        'versions.description',
                        'versions.year',
                        'versions.doors',
                        'showroom_has_versions.order_by',
                        'versions.motorization_id',
                        'versions.cc_id',
                        'versions.doors',
                        'versions.year',
                        'versions.fuels_id',
                        'versions.bodies_id',
                        'versions.transmissions_id'
                    )
                    ->with([
                        'cc' => function($q) {
                            $q->select('*');
                        },
                        'fuel' => function($q) {
                            $q->select('*');
                        },
                        'body' => function($q) {
                            $q->select('*');
                        },
                        'motorization' => function($q) {
                            $q->select('*');
                        },
                        'transmission' => function($q) {
                            $q->select('*');
                        },
                        'colors' => function($q) {
                            $q->select(
                                 'versions_id', 
                                 'hexadecimal', 
                                 DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'), 
                                 'color'
                            );
                        }
                    ])
                    ->whereNotNull('large_image')
                    ->orderBy('showroom_has_versions.order_by','asc');
                }
            ]);
        }

        // $query->join('models', 'showroom.models_id', '=', 'models.id');
        
        $query->where([
                ['showroom.type', '=', 'novo'],
                ['showroom.active', '=', 1],
                ['showroom.companies_id', '=', self::$COMPANY_ID]
            ]);

        return $query;
    }

    public static function customFetchAllNovos($type_vehicle = null, $order = [], $limit = null, $page = null, $idsOutside = null )
    {
        $query = self::customBuildQueryNovos();
        
        if( !empty($type_vehicle) )
        $query->where('showroom.type_id', '=', $type_vehicle);
        
        self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();
        self::singleCustomKeyWithNoSQLQuery($dados);
        $data = self::customOrderBy($dados, 'posicao');
        
        return $data;
    }

    public static function customFetchAllNovosIndex($type_vehicle = null, $order = [], $limit = null, $page = null, $idsOutside = null )
    {
        $query = self::customBuildQueryNovosIndex();
        
        if( !empty($type_vehicle) )
        $query->where('showroom.type_id', '=', $type_vehicle);
        
        self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();
        self::singleCustomKeyWithNoSQLQuery($dados);
        $data = self::customOrderBy($dados, 'posicao');
        
        return $data;
    }

    public static function customOrderBy($query, $campo)
    {

        $dados = [];
        foreach ($query as $key => $value) {
            if (!isset($value->{$campo})){
                $value->{$campo} = 999;
            }
            $dados[] = $value;
        }

        foreach ($dados as $k => $row) {
            $aux[$k] = $row["{$campo}"];
        }
        
        array_multisort($aux, SORT_ASC, $dados);
        return $dados;
    }

    public static function customNovoBySlug($slug)
    {
    	$query = self::customBuildQueryNovo(true);
        $query->whereHas('model', function($q) use($slug) {
            $q->where('models.slug', '=', $slug);
        });
        $novo = $query->first();

        if($novo){

            self::singleCustomKey($novo);
            
            $novo->numerodeversoes = !empty($novo->versoes) ? count($novo->versoes) : 0;

            $totalofertas = self::query()
                ->where([
                    ['type', '=', 'oferta'],
                    ['models_id', '=', $novo->models_id],
                    ['active', '=', 1],
                    ['companies_id', '=', self::$COMPANY_ID],
                    ['finish_at', '>=', date('Y-m-d')]
                ])
                ->count();

            $novo->totalofertas = $totalofertas;

            if( $novo->versoes ){
                foreach ($novo->versoes as $version) {
                    $version->full_name = $novo->model->complete_name.' '.$version->complete_name;
                }
            }

        }

        return $novo;

    }

    private static function basicCustomBuildQuerySeminovos()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.color',
                'showroom.model_year',
                'showroom.fab_year',
                'showroom.km',
                'showroom.plaque',
                'showroom.used_value',
                'showroom.models_id',
                'showroom.versions_id'
            ]);

        self::basicContainShowroomOferta($query);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'seminovo']
            ]);

        return $query;
    }

    private static function customBuildQuerySeminovos()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.color',
                'showroom.model_year',
                'showroom.fab_year',
                'showroom.km',
                'showroom.plaque',
                'showroom.used_value',
                'showroom.models_id',
                'showroom.versions_id'
            ]);

        self::containShowroomOferta($query);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'seminovo']
            ]);

        return $query;
    }

    public static function customListAllSeminovos($order = [], $limit = null, $page = null, $idsOutside = null)
    {
    	$query = self::basicCustomBuildQuerySeminovos();
    	self::customPaginate($query, $order, $limit, $page, $idsOutside);

        if($limit){
            $query->limit($limit);
        }

        $dados = $query->get();
        foreach($dados as $seminovo) {
            self::singleCustomKey($seminovo);
        }

        return $dados;
    }

    public static function customSeminovoBySlug($slug)
    {
    	$query = self::customBuildQuerySeminovos();
        $query->where('showroom.slug', '=', $slug);
        $seminovo = $query->first();

        if($seminovo){
	        self::singleCustomKey($seminovo);
        }

        return $seminovo;
    }

    public static function customSeminovoBySlugId($slug)
    {
    	$query = self::customBuildQuerySeminovos();
        $query->where('showroom.id', '=', $slug);
        $seminovo = $query->first();

        if($seminovo){
	        self::singleCustomKey($seminovo);
        }

        return $seminovo;
    }

    private static function customBuildQueryConsorcios()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.brand_new_value',
                'showroom.slug',
                'showroom.color',
                'showroom.type'
            ])
            ->with([
                'model' => function($q) {
                    $q->select( 
                        'id',
                        'name',
                        'complete_name',
                        'slug',
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
                    );
                    // ->with([
                    //     'brand' => function($q) {
                    //         $q->select('id', 'name');
                    //     }
                    // ]);
                },
                // 'optionals' => function($q) {
                //     $q->select(
                //         'id',
                //         'value',
                //         'icon',
                //         'svg'
                //     );
                // },
                // 'custom_answers' => function($q) {
                //     $q->with([ 'custom_question' => function($q) {
                //             $q->select([
                //                 'custom_questions.id',
                //                 'custom_questions.slug',
                //                 'custom_questions.type'
                //             ]);
                //         }
                //     ]);
                // },
                // 'consortium_parcels' => function($q) {
                //     $q->select(
                //         'showroom_has_consortium_parcels.parcel_value',
                //         'consortium_parcels.parcel',
                //         'consortium_parcels.consortium_id'
                //     )
                //     ->with([
                //         'consortium'
                //     ]);
                // }
            ]);

        $query->where([
            ['showroom.companies_id', self::$COMPANY_ID],
            ['showroom.active', 1],
            ['showroom.type', 'consorcio']
        ]);

        return $query;
    }

    public static function customFetchAllConsorcios($order = [], $limit = null, $page = null, $idsOutside = null)
    {
    	$query = self::customBuildQueryConsorcios();
    	self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();

        // $dadosPlanos = [];
        // $planosName = [];
        // foreach ($dados as $consorcio) {
        //     self::singleCustomKey($consorcio);

        //     // foreach($consorcio->consortium_parcels as $plan){
        //     //     $dadosPlanos[$plan->consorcium->name] = 

        //     // }

        //     $itemsParcels = $consorcio->consortium_parcels->filter(function($item){
        //         return $item->parcel_value != null && $item->parcel_value > 0;
        //     });

        //     $consorcio->parcel_value = $itemsParcels->min('parcel_value');

            
        // }

        return $dados;

    }

    public static function customConsorcioBySlug($slug)
    {
    	$query = self::customBuildQueryConsorcios();
        $query->where('showroom.slug', '=', $slug);
        $consorcio = $query->first();

        if($consorcio)
	        self::singleCustomKey($consorcio);
        $ordenando = [];
        $planos = [];
        // foreach ($consorcio->consortium_parcels as $key => $consortium) {
        //     $ordenando[$key] = ['parcel_value' => (int)$consortium->parcel_value];
        //     $planos[$consortium->consortium_id][] = $consortium;
        // }
        $aux = collect($ordenando)->sortBy('parcel_value');
        $aux->values()->all();
        $consorcio->menorValor = $aux->first()['parcel_value'];
        $consorcio->planos = $consorcio->consortium_parcels->groupBy('consortium_id');
        
        return $consorcio;
    }

    private static function customBuildQueryFinanciamentos()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.type',
                'showroom.brand_new_value',
                'showroom.plaque_license',
                'showroom.financing_taxes',
                'showroom.models_id',
                'showroom.versions_id'
            ]);

        self::containShowroomOferta($query);

        $query->with([
            'financing_table' => function($q) {
                return $q->select([
                    '*'
                ])->with([
                    'financing_table_parcels' => function($q) {
                        return $q->select([
                            // 'financing_table_parcels.financing_table_id',
                            // 'financing_table_parcels.parcel',
                            // 'financing_table_parcels.tax',
                            '*'
                        ]);
                    }
                ]);
            }
        ]);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'financiamento']
            ]);

        return $query;
    }

    public static function customFetchAllFinanciamentos($order = [], $limit = null, $page = null, $idsOutside = null)
    {
    	$query = self::customBuildQueryFinanciamentos();
    	self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();

        foreach($dados as $financiamento) {
            self::singleCustomKey($financiamento);
        }

        return $dados;
    }

    public static function customFinanciamentoBySlug($slug)
    {
    	$query = self::customBuildQueryFinanciamentos();
        $query->where('showroom.slug', '=', $slug);
        $financiamento = $query->first();

        if($financiamento)
	        self::singleCustomKey($financiamento);

        return $financiamento;
    }

    private static function customBuildQueryVendasDiretas()
    {
        $query = self::query()
            ->select([
                'showroom.id',
                'showroom.slug',
                'showroom.description',
                'showroom.corporate_sales_category',
                'showroom.value_from',
                'showroom.models_id',
                'showroom.versions_id',
                'showroom.type',
                'showroom.colors_id'
            ]);

        self::containShowroomOferta($query);

        $query->where([
                ['showroom.companies_id', self::$COMPANY_ID],
                ['showroom.active', 1],
                ['showroom.type', 'vendas_diretas']
            ]);

        return $query;
    }

    public static function customFetchAllVendasDiretas($order = [], $limit = null, $page = null, $idsOutside = null)
    {
    	$query = self::customBuildQueryVendasDiretas();
    	self::customPaginate($query, $order, $limit, $page, $idsOutside);
        $dados = $query->get();
        foreach($dados as $venda_direta) {
            self::singleCustomKey($venda_direta);
        }

       /* $dados = collect($dados->filter(function($showroom){
            $date = str_replace('/', '-', $showroom->finish_at);
            if(strtotime($date) >= strtotime(date('d-m-Y'))){
                return $showroom;
            }
        })->values()->all());*/

        return $dados;
    }

    public static function customVendaDiretaBySlug($slug)
    {
    	$query = self::customBuildQueryVendasDiretas();
        $query->where('showroom.slug', '=', $slug);
        $venda_direta = $query->first();

        if($venda_direta)
	        self::singleCustomKey($venda_direta);

        return $venda_direta;
    }

    public static function customVendaDiretaById($id)
    {
    	$query = self::customBuildQueryVendasDiretas();
        $query->where('showroom.id', '=', $id);
        $venda_direta = $query->first();

        if($venda_direta)
	        self::singleCustomKey($venda_direta);

        return $venda_direta;
    }

    public static function customDiffVendaDiretaSlug($slug)
    {
        $query = self::customBuildQueryVendasDiretas();
        $query->where('showroom.slug', '!=', $slug);
        $dados = $query->get();

        foreach($dados as $venda_direta) {
            self::singleCustomKey($venda_direta);
        }

        return $dados;
    }

    public static function customListMarcasByType($showroom_type=null, $toSiteForm=false)
    {
        $fields = [
            'id' => 'brands.id',
            'name' => 'brands.name',
        ];
        
        if( $toSiteForm )
            $fields['id'] = $fields['name'];

        $conditions = [
        	['showroom.active', '=', 1],
            ['showroom.companies_id', '=', self::$COMPANY_ID]
       	];

        if( !empty($showroom_type) )
            $conditions[] = ['showroom.type', '=', $showroom_type];
        else
        	$conditions[] = ['showroom.type', '=', 1];

        $query = self::query()
            ->select($fields)
            ->join('models', 'showroom.models_id', '=', 'models.id')
            ->join('brands', 'models.brands_id', '=', 'brands.id')
            ->where($conditions)
           	->groupBy(['brands.id', 'brands.name'])
           	->orderBy('brands.name', 'asc');

        $dados = $query->get();
        $dadosPlucked = $dados->pluck('name', ($fields['id'] == $fields['name'] ? 'name' : 'id'));

        return $dadosPlucked->all();
    }

    public static function customListModelos($showroom_type, $toSiteForm=false)
    {
        $fields = [
            'id' => 'models.id',
            'name' => 'models.name',
        ];
        
        if( $toSiteForm )
            $fields['id'] = $fields['name'];

        $conditions = [
        	['showroom.active', '=', 1],
            ['showroom.companies_id', '=', self::$COMPANY_ID]
       	];

        if( !empty($showroom_type) )
        	$conditions[] = ['showroom.type', '=', $showroom_type];

        $query = self::query()
            ->select($fields)
            ->join('models', 'showroom.models_id', '=', 'models.id')
            ->where($conditions)
           	->groupBy(['models.id', 'models.name'])
           	->orderBy('models.name', 'asc');

        $dados = $query->get();
        $dadosPlucked = $dados->pluck('name', ($fields['id'] == $fields['name'] ? 'name' : 'id'));

        return $dadosPlucked->all();

    }

    public static function customListNovos()
    {
        $query = self::query()
            ->select('models.slug', 'models.complete_name')
            ->join('models', 'showroom.models_id', '=', 'models.id')
            ->where([
                ['showroom.active', '=', 1],
                ['showroom.companies_id', '=', self::$COMPANY_ID],
                ['showroom.type', '=', 'novo']
           	])
           	->groupBy(['models.slug', 'models.complete_name'])
           	->orderBy('models.complete_name', 'asc');

        $dados = $query->get();
        $dadosPlucked = $dados->pluck('complete_name', 'slug');

        return $dadosPlucked->all();

    }

    public static function customMapNovos()
    {
        $query = self::query()
            ->select([
                'models.slug', 
                'models.complete_name',
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image')
            ])
            ->join('models', 'showroom.models_id', '=', 'models.id')
            ->where([
                ['showroom.active', '=', 1],
                ['showroom.companies_id', '=', self::$COMPANY_ID],
                ['showroom.type', '=', 'novo']
            ])
            ->groupBy(['models.slug', 'models.complete_name', 'models.image'])
            ->orderBy('models.complete_name', 'asc');

        return $query->get();
    }


     public static function customFetchAllSlides() 
    {

        $questions_id_oferta_slider = 2063;
        $questions_id_oferta_value = 447;
        $slides = self::query()
            
                    ->select([
                        'showroom.id',
                        'showroom.slug',
                        'showroom.description',
                        'showroom.payment_method',
                        'showroom.color',
                        'showroom.parcel_amount',
                        'showroom.parcel_value',
                        'showroom.entry',
                        'showroom.bonus',
                        'showroom.value_from',
                        'showroom.value_to',
                        'showroom.finish_at',
                        'showroom.type',
                        'showroom.color',
                        'showroom.brand_new_value',
                        'showroom.models_id',
                        'showroom.versions_id',
                        DB::raw('(SELECT ((COUNT(DISTINCT showroom_visits.ip) + 1) * 3) FROM showroom_visits WHERE showroom_visits.showroom_id = showroom.id) as total_visualizacoes'),
                        DB::raw('((SELECT COUNT(DISTINCT custom_answer.id) FROM custom_answer WHERE custom_answer.value = "' . $questions_id_oferta_value . '" AND custom_answer.showroom_id = showroom.id AND custom_answer.custom_questions_id = ' . $questions_id_oferta_slider . ') > 0) as slider')
                    ])
                    ->with([ 
                        'model' => function($q) {
                            $q->select( 
                                '*',
                                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", models.image) as image')
                            )
                            ->with([
                                'brand' => function($q) {
                                    $q->select('brands.id', 'brands.name');
                                }
                            ]);
                        },
                        'version' => function($q) {
                            $q->select(
                                'id',
                                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", large_image) as image'),
                                'name',
                                'complete_name',
                                'slug'
                            );
                        },
                        'custom_answers' => function($q) {
                            $q->with([ 'custom_question' => function($q) {
                                $q->select([
                                    'custom_questions.id',
                                    'custom_questions.slug',
                                    'custom_questions.type'
                                ]);
                            }]);
                        }
                    ])->where([
                        ['showroom.active', 1],
                        ['companies_id', self::$COMPANY_ID],
                        ['type', 'oferta'],
                    ])

            ->orderBy('id', 'desc')
            ->get()
            ->filter(function($value, $key){
                return ($value->slider > 0 && $value->finish_at >= date('Y-m-d 23:59:59')) ;
            });

        foreach($slides as $slide) {
            self::singleCustomKey($slide);
        }

        return $slides;


    }

}