<?php

namespace App\Models;

class Cc extends AppDescomplicarModel
{
	protected $table = 'cc';
	
    public function type()
	{
	    return $this->belongsTo('App\Models\Type', 'type_id', 'id');
	    // return $this->belongsTo('App\Models\Type', 'type_id', 'id')->withDefault();
	}

	public function versions()
	{
		// return $this->hasMany('App\Comment', 'foreign_key', 'local_key');
		return $this->hasMany('App\Models\Version', 'cc_id', 'id');
	}
}
