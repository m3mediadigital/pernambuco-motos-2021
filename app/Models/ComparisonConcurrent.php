<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class ComparisonConcurrent extends AppDescomplicarModel
{
	protected $table = 'comparisons_concurrents';

    public function model()
	{
	    return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}
    
    public function version()
    {
        return $this->belongsTo('App\Models\Version', 'versions_id', 'id');
    }
    
    public function comparison()
    {
        return $this->belongsTo('App\Models\Comparison', 'comparisons_id', 'id');
    }
	
    public function comparison_question()
	{
	    return $this->belongsTo('App\Models\ComparisonQuestion', 'comparisons_questions_id', 'id');
	}

}
