<?php

namespace App\Models;

class Coupon extends AppDescomplicarModel
{
	protected $table = 'coupons';
	
    public function showroom()
	{
	    return $this->belongsTo('App\Models\Showroom', 'showroom_id', 'id');
	}
}
