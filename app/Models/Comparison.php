<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Comparison extends AppDescomplicarModel
{
	protected $table = 'comparisons';

    public function model()
	{
	    return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}
    
    public function version()
    {
        return $this->belongsTo('App\Models\Version', 'versions_id', 'id');
    }
    
    public function comparison_concurrents()
    {
        return $this->hasMany('App\Models\ComparisonConcurrent', 'comparisons_id', 'id');
    }
	
    public function comparison_questions()
	{
	    return $this->hasMany('App\Models\ComparisonQuestion', 'comparisons_id', 'id');
	}

    public static function comparate(&$showroom){

        $versions_array = [];
        if(isset($showroom->versoes) && !empty($showroom->versoes)){
            foreach($showroom->versoes as $v){
                $versions_array [] = $v->id;
            }
        }else{
            $versions_array [] = $showroom->version->id;
        }

        $query = self::query()
            ->select('id', 'models_id', 'versions_id');

        $query->with([
            'model' => function($q) {
                $q->select( 
                    'id',
                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                    'name',
                    'complete_name',
                    'slug',
                    'description',
                    'brands_id'
                )
                ->with([
                    'brand' => function($q) {
                        $q->select('id', 'name');
                    }
                ]);
            },
            'version' => function($q) {
                $q->select(
                    'id',
                    DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", large_image) as image'),
                    'name',
                    'complete_name',
                    'slug'
                );
            },
            'comparison_questions' => function ($q){
                $q->select(
                   'id',
                   'title',
                   'answer',
                   'comparisons_id'
                );
            },
            'comparison_concurrents' => function ($q){
                $q->select(
                   'id',
                   'answer',
                   'equal',
                   'comparisons_id',
                   'models_id',
                   'versions_id',
                   'comparisons_questions_id'
                )->with([
                    'model' => function($q) {
                        $q->select( 
                            'id',
                            DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                            'name',
                            'complete_name',
                            'slug',
                            'description'
                        )
                        ->with([
                            'brand' => function($q) {
                                $q->select('id', 'name');
                            }
                        ]);
                    },
                    'comparison_question' => function($q) {
                        $q->select(
                            'id',
                            'title',
                            'answer'
                        );
                    },
                    'version' => function($q) {
                        $q->select(
                            'id',
                            DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", IFNULL(silhouette_image, large_image)) as image'),
                            'name',
                            'complete_name',
                            'slug',
                            'silhouette_image', 'large_image'
                        );
                    }

                ]);
            }
        ]);

        $query->where('companies_id', '=', self::$COMPANY_ID);
        $query->whereIn('versions_id', $versions_array);

        $dados = $query->get();
        return $dados;

    }

}
