<?php

namespace App\Models;

class Consortium extends AppDescomplicarModel
{
	protected $table = 'consortium';
	
	public function consortium_parcels()
	{
		return $this->hasMany('App\Models\ConsortiumParcel', 'consortium_id', 'id');
	}
}
