<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;

class Testimonial extends AppDescomplicarModel
{
	protected $table = 'testimonials';
	
    public static function customFetchAll() 
    {
        return self::query()
        	->select(
                // DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", image) as image'),

                'testimony',
                'name',
                'address'
            )
            ->where([
                ['companies_id', self::$COMPANY_ID],
                ['active', 1],
            ])
            ->inRandomOrder()
        	->get();
    }

}
