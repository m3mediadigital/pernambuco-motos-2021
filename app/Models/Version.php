<?php

namespace App\Models;

class Version extends AppDescomplicarModel
{
	protected $table = 'versions';
	
    public function optionals()
	{
	    return $this->belongsToMany('App\Models\Optional', 'versions_has_optionals', 'versions_id', 'optionals_id');
	}

    public function colors()
	{
		return $this->hasMany('App\Models\Color', 'versions_id', 'id');
	}

	public function model()
	{
		return $this->belongsTo('App\Models\Model', 'models_id', 'id');
	}
	
	public function body()
	{
		return $this->belongsTo('App\Models\Body', 'bodies_id', 'id');
	}
	
	public function cc()
	{
		return $this->belongsTo('App\Models\Cc', 'cc_id', 'id');
	}
	
	public function motorization()
	{
		return $this->belongsTo('App\Models\Motorization', 'motorization_id', 'id');
	}
	
	public function transmission()
	{
		return $this->belongsTo('App\Models\Transmission', 'transmissions_id', 'id');
	}
	
	public function fuel()
	{
		return $this->belongsTo('App\Models\Fuel', 'fuels_id', 'id');
	}

	public function getCompleteNameAttribute($value)
	{
		$value = explode(" ", $value);
		foreach($value as $k => $v) {
			$v = strtolower($v);

			switch($v) {
				case "manual":
				case "sedan":
				case "hatch":
					unset($value[$k]);
					break;

				case "automatico":
				case "automático":
				case "automatizado":
					$value[$k] = 'AT';
					break;
			}
		}

		$value = trim(implode(" ", $value));

		return $value;
	}

}
