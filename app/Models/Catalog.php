<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\LengthAwarePaginator;

class Catalog extends AppDescomplicarModel
{
	protected $table = 'catalog';
	
    public function models()
	{
	    return $this->belongsToMany('App\Models\Model', 'catalog_has_models', 'catalog_id', 'models_id');
	}

    public function category()
    {
        return $this->belongsTo('App\Models\CatalogCategory', 'catalog_categories_id', 'id');
    }

    public function custom_answers()
    {
        return $this->hasMany('App\Models\CustomAnswer', 'catalog_id', 'id');
    }

    private static function customBuildQuery($custom_answer = true)
    {
        return self::query()
            ->select(
                'catalog.id',
                'catalog.models_id',
                DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", catalog.image) as image'),
                'catalog.name',
                'catalog.slug',
                'catalog.ano',
                'catalog.description',
                'catalog.price',
                'catalog.universal',
                'catalog.catalog_categories_id'
            )
            ->with([
                'models' => function($q) {
                    $q->select( 
                        DB::raw('CONCAT("https://descomplicar.s3-sa-east-1.amazonaws.com/upload/", models.image) as image'),
                        'models.name',
                        'models.complete_name',
                        'models.slug'
                    )
                    ->with([
                        'brand' => function($q) {
                            $q->select('brands.name');
                        }
                    ]);
                },
                'category' => function($q) {
                    $q->select('*')
                    ->orderBy('catalog_categories.slug', 'desc');
                },
                'custom_answers' => function($q) {
                    $q->with([
                        'custom_options', 
                        'custom_question' => function($q) {
                            $q->select([
                                'custom_questions.id',
                                'custom_questions.slug',
                                'custom_questions.type'
                            ]);
                        }
                    ]);
                }
            
            ])
            // ->join('catalog_has_models', function ($join) {
            //     $join->on('catalog.id', '=', 'catalog_has_models.catalog_id');
            // })
            ->where([
                ['catalog.companies_id', self::$COMPANY_ID],
            ])->inRandomOrder();
    }

    public function scopeIsService($query)
    {
        return $query->whereHas('category', function($q) {
            return $q->where('slug', 'servicos');
        });
    }

    public function scopeIsActive($query)
    {
        return $query->whereHas('custom_answers', function($q) {
            return $q->whereHas('custom_question', function($q) {
                    return $q->where('slug', 'ativo');
                })
                ->whereHas('custom_options', function($q) {
                    return $q->where('custom_option', 'Sim');
                });
        });
    }

    public function scopeIsInTime($query)
    {
        return $query->whereHas('custom_answers', function($q) {
            return $q->whereHas('custom_question', function($q) {
                    return $q->where('slug', 'finish_at');
                })
                ->where('value', '>', date('Y-m-d H:i:s'));
        });
    }

    public static function customFetchAllServices($paginate = false) 
    {
        $query = self::customBuildQuery();
        $dados = $query
            ->isService()
            ->isActive()
            // ->isInTime()
            ->orderBy('catalog.name', 'asc')->get();

        self::singleCustomKeyWithNoSQLQuery($dados);
        // Pega filtros
        $filtros['modelos'] = [];
        foreach ($dados as $item) {
            $modelos_linha = [];
            foreach($item->models as $modeloItem) {
                $filtered = false;
                foreach ($modelos_linha as $key => $value) {
                    if( preg_match('/^'.$key.'(-[\d\-]+)$/', $modeloItem->slug) ){
                        $filtered = true;
                        break;
                    }
                }
                if( !$filtered ){
                    $modelos_linha[$modeloItem->slug] = $modeloItem->name;
                    $filtros['modelos'][$modeloItem->slug] = $modeloItem->name;
                }
            }
            // self::singleCustomKey($item);
            // self::singleCustomKeyWithNoSQLQuery($item);
            $item->modelos_linha = $modelos_linha;
        }

        $dados = collect($dados->filter(function($item){
            $date = str_replace('/', '-', $item->finish_at);
            if(strtotime($date) >= strtotime(date('d-m-Y'))){
                return $item;
            }
        })->values()->all());

        if($paginate){
            $dados = new LengthAwarePaginator(
                    $dados->slice((LengthAwarePaginator::resolveCurrentPage() * $paginate) - $paginate, $paginate)->all(), count($dados), $paginate, null, ['path' => url()->current()]
                );
        }
        return $dados;
    }

    public static function customFetchAll($limit=null) 
    {
        $query = self::customBuildQuery();

        $dados = $query
            ->orderBy('catalog.catalog_categories_id', 'desc');

        if($limit) {
            $query->limit($limit);
        }

        $dados = $query->get();

        return $dados;
    }

    public static function customFetchAllCategories() 
    {
        $query = self::customBuildQuery();
        // self::genericCustomPaginate($query, $order, $limit, $page);

        $dados = $query
            ->orderBy('catalog.catalog_categories_id', 'desc')
            ->get();

        $dados = $dados->filter(function($item){
            if($item->category->slug != 'servicos')
                return $item;
        })->values()->all();

        $dados = collect($dados);
        
        $categories = [];

        foreach($dados as $k => $v) {
            $modelos_linha = [];

            if(isset($v->models) && count($v->models) > 0) {
                foreach($v->models as $modelo) {
                    $modelos_linha[] = $modelo->name;
                }
            }

            if( !empty($v->category) )
                $categories[ $v->category->slug ] = $v->category->value;

            $dados[$k]['modelos_linha'] = $modelos_linha;
            unset($v->models);
        }

        return $categories;
    }

    public static function customBySlug($slug)
    {
        $query = self::customBuildQuery();
        $catalog = $query
            ->where('catalog.slug', '=', $slug)
            ->first();

        if( $catalog ){

            $modelos_linha = [];

            if(isset($catalog->models) && count($catalog->models) > 0) {
                foreach($catalog->models as $modelo) {
                    $modelos_linha[] = $modelo->name;
                }
            }

            $catalog->modelos_linha = $modelos_linha;
            unset($catalog->models);

            self::singleCustomKey($catalog);
        }

        return $catalog;
    }

    public static function customById($id)
    {
        $query = self::customBuildQuery();
        $catalog = $query
            ->where('catalog.catalog_categories_id', '=', $id)
            ->get();

        if( $catalog ){

            $modelos_linha = [];

            if(isset($catalog->models) && count($catalog->models) > 0) {
                foreach($catalog->models as $modelo) {
                    $modelos_linha[] = $modelo->name;
                }
            }

            $catalog->modelos_linha = $modelos_linha;
            unset($catalog->models);

            self::singleCustomKey($catalog);
        }

        return $catalog;
    }

    public static function customDiffSlug($slug = null, $limit)
    {
        $query = self::customBuildQuery();
        $catalog = $query
            ->where('catalog.slug', '!=', $slug)
            ->limit($limit)
            ->inRandomOrder()
            ->get();

        return $catalog;
    }

    public static function customDiffSlugServices($slug = null, $limit)
    {
        $query = self::customBuildQuery();
        $catalog = $query
            ->where('catalog.slug', '!=', $slug)
            ->limit($limit)
            ->inRandomOrder()
            ->get();

        $catalog = $catalog->filter(function($item){
            if($item->category->slug == 'servicos')
                return $item;
        })->values()->all();

        $catalog = collect($catalog);

        self::singleCustomKey($catalog);

        return $catalog;
    }

    public static function customPagination($modelo, $ordem, &$paginacao, &$filtros=null)
    {
        if( !is_numeric($paginacao['atual']) || $paginacao['atual'] < 1 )
            $paginacao['atual'] = 1;
        else
            $paginacao['atual'] = intval($paginacao['atual']);

        $paginacao['query_string_ord'] = '?';
        $paginacao['query'] = [];
        $resultadoGeral = [];
        $filtros = [];

        // Ordena
        $ordField = 'catalog.name';
        $ordDirection = 'asc';

        if( !empty($ordem) ){
            $paginacao['query']['ordem'] = $ordem;
            if( preg_match('/^nome-(asc|desc)$/', $ordem, $match) ){
                $ordField = 'catalog.name';
                $ordDirection = $match[1];
            }else if( preg_match('/^preco-(asc|desc)$/', $ordem, $match) ){
                $ordField = 'catalog.price';
                $ordDirection = $match[1];
            }else{
                unset($paginacao['query']['ordem']);
            }
        }

        $todos = self::customBuildQuery()
            ->orderBy($ordField, $ordDirection)
            ->get();


        $todos = $todos->filter(function($item){
            if($item->category->slug != 'servicos')
                return $item;
        })->values()->all();

        $todos = collect($todos);

        // Pega filtros
        self::singleCustomKeyWithNoSQLQuery($todos);
        $filtros['modelos'] = [];
        foreach ($todos as $item) {
            $modelos_linha = [];
            foreach($item->models as $modeloItem) {
                $filtered = false;
                foreach ($modelos_linha as $key => $value) {
                    if( preg_match('/^'.$key.'(-[\d\-]+)$/', $modeloItem->slug) ){
                        $filtered = true;
                        break;
                    }
                }
                if( !$filtered ){
                    $modelos_linha[$modeloItem->slug] = $modeloItem->name;
                    $filtros['modelos'][$modeloItem->slug] = $modeloItem->name;
                }
            }
            // self::singleCustomKey($item);
            $item->modelos_linha = $modelos_linha;
        }

        //dd($todos);
        $todos = $todos->where('ativo', 'Sim');

        $todos = collect($todos->filter(function($item){
            $date = str_replace('/', '-', $item->finish_at);
            if(strtotime($date) >= strtotime(date('d-m-Y'))){
                return $item;
            }
        })->values()->all());
        
        // Filtra modelos
        if( !empty($modelo) ){
            $paginacao['query_string_ord'] .= 'modelo='.$modelo.'&';
            $paginacao['query']['modelo'] = $modelo;
            $modelos = $todos->filter(function ($value, $key) use ($modelo){
                $filtered = false;
                foreach ($value->modelos_linha as $ikey => $ivalue) {
                    if( preg_match('/^'.$ikey.'(-[\d\-]+)$/', $modelo) ){
                        $filtered = true;
                        break;
                    }
                }
                return $value->universal || ($value->modelos_linha && ($filtered || array_key_exists($modelo, $value->modelos_linha)));
            });
            $resultadoGeral = $modelos;
        }else{
            $resultadoGeral = $todos;
        }


        $resultadoChunked = $resultadoGeral->chunk($paginacao['itens']);

        // Ajusta paginacao
        $paginacao['paginas'] = count($resultadoChunked);
        if( $paginacao['atual'] > $paginacao['paginas'] )
            $paginacao['atual'] = 1;
        
        $paginacao['query']['pagina'] = '';
        $paginacao['query_string'] = http_build_query($paginacao['query']);
        $paginacao['query_string_ord'] .= 'ordem=';

        if( $resultadoChunked->count() == 0 )
            return [];
        // return $resultadoChunked[ $paginacao['atual']-1 ];
        return $resultadoChunked[ $paginacao['atual']-1 ];
    }


}
