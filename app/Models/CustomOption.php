<?php

namespace App\Models;

class CustomOption extends AppDescomplicarModel
{
	protected $table = 'custom_options';
	
    public function custom_question()
	{
	    return $this->belongsTo('App\Models\CustomQuestion', 'custom_questions_id', 'id');
	}

}
