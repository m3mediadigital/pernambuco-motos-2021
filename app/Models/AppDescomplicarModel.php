<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppDescomplicarModel extends Model
{
	const ACTIVE = 1;
	static $COMPANY_ID = 40;
    static $COMPANY_KEY = 'e03ef57fe030adbe8c4b31ee6837eaf6';

	public function __construct(array $attributes = array())
	{
	    parent::__construct($attributes);
        if(!self::$COMPANY_ID || !is_int(self::$COMPANY_ID)) {
            throw new \Exception("O ID da empresa informado é inválido.");
        }
	}

    protected static function singleCustomKey(&$carro)
    {
        if($carro && !empty($carro->custom_answers)) {
            foreach($carro->custom_answers as $k => $custom_answer) {
                $campo = $custom_answer->custom_question->slug;

                if($custom_answer->custom_question->type == 'select') {
                    $option = CustomOption::query()
                        ->select('custom_option')
                        ->where([
                            ['custom_options.custom_questions_id', '=', $custom_answer->custom_question->id],
                            ['custom_options.id', '=', $custom_answer->value],
                        ])
                        ->first();
                    $carro->$campo = $option->custom_option;

                } else if(in_array($custom_answer->custom_question->type, ['checkbox', 'select_image'])) {
                    $pre = '';
                    if($custom_answer->custom_question->type == 'select_image') {
                        $pre = 'https://descomplicar.s3-sa-east-1.amazonaws.com/upload/';
                    }
                    $option = CustomOption::query()
                        ->select('custom_option')
                        ->where('custom_options.custom_questions_id', '=', $custom_answer->custom_question->id)
                        ->whereIn('custom_options.id', unserialize($custom_answer->value) )
                        ->get();
                    $opts = array();
                    foreach($option as $opt) {
                        $opts[] = $pre . $opt->custom_option;
                    }
                    $carro->$campo = $opts;

                } else if(in_array($custom_answer->custom_question->type, ['image', 'album_images']) && trim($custom_answer->value) != '') {
                    $carro->$campo = 'https://descomplicar.s3-sa-east-1.amazonaws.com/upload/' . $custom_answer->value;

                } else if($custom_answer->custom_question->type == 'money') {
                    $money = str_replace('.', '', $custom_answer->value);
                    $money = str_replace(',', '.', $money);
                    $carro->$campo = (float)$money;
                } else {
                    $carro->$campo = $custom_answer->value;
                }
            }

            unset($carro->custom_answers);
        }

        $optionals_array = [];
        if($carro && !empty($carro->optionals)) {
            foreach($carro->optionals as $k => $opt) {
                $slug = mb_convert_case( preg_replace('/(\s+)/', '-', $opt->value), MB_CASE_LOWER, 'utf-8' );
                // Falta tirar acentos e caracteres especiais...
                $carro->optionals[$k]->slug = $slug;
                $optionals_array[ $carro->optionals[$k]->slug ] = $opt->value;
            }
            unset($carro->optionals);
        }
        $carro->optionals = $optionals_array;

    }

    /**
     * Faz o mesmo que o singleCustomKey, mas sem chamadas ao banco de dados.
     *
     * @param [type] $ofertas
     * @return void
     */
    protected static function singleCustomKeyWithNoSQLQuery(&$ofertas) {
        foreach($ofertas as $oferta) {
            foreach($oferta->custom_answers as $answer) {
                switch ($answer->custom_question->type) {
                    case 'select':
                        if(isset($answer->custom_question) && isset($answer->custom_options)) {
                            $oferta[$answer->custom_question->slug] = $answer->custom_options->custom_option;
                        }
                        break;

                    case 'select_image':
                        $campo = $answer->custom_question->slug;
                        $pre = 'https://descomplicar.s3-sa-east-1.amazonaws.com/upload/';
                        
                        $option = CustomOption::query()
                            ->select('custom_option')
                            ->where('custom_options.custom_questions_id', '=', $answer->custom_question->id)
                            ->whereIn('custom_options.id', unserialize($answer->value) )
                            ->get();
                        $opts = [];
                        foreach($option as $opt) {
                            $opts['image'] = $pre . $opt->custom_option;
                        }
                        $oferta->$campo = $opts;
                        break;
                    default:
                        if(isset($answer->custom_question)) {
                            $oferta[$answer->custom_question->slug] = $answer->value;
                        }
                        break;
                }
            }
        }
    }

    protected static function genericCustomPaginate(&$query, $order = [], $limit = null, $page = null)
    {
        if($order){
            if( is_array($order) && count($order) == 2 ){
                list($field, $ord) = $order;
                $query->orderBy($field, $ord);
            }else if( $order == 'RAND' ){
                $query->inRandomOrder();
            }
        }

        if($limit && is_numeric($limit)) {
            $query->limit($limit);
            
            if($page && is_numeric($page)) {
                $query->offset( (($page-1)*$limit) );
            }
        }

    }


}
