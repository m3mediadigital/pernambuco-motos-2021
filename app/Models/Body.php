<?php

namespace App\Models;

class Body extends AppDescomplicarModel
{
	protected $table = 'bodies';
	
    public function type()
	{
	    return $this->belongsTo('App\Models\Type', 'type_id', 'id');
	    // return $this->belongsTo('App\Models\Type', 'type_id', 'id')->withDefault();
	}
}
