<?php

namespace App\Traits;

use App\Mail\Contato;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\AgendamentoBlackFridayRequest;
use Illuminate\Support\Facades\Storage;

trait Email
{
    private $_connectlead_key = 'Basic YXV0b2ZvcmNlQGl0YWxpYW5hOjFlNmJhOA==';
	private function &getValidator($inputs, $rules = [])
	{
		Validator::extend('phone_br', function ($attribute, $value, $parameters, $validator) {
			$onlyNumbers = preg_replace('/([^\d]+)/', '', $value);
		    return in_array(strlen($onlyNumbers), [10,11]);
		});

		Validator::extend('cpf', function ($attribute, $value, $parameters, $validator) {
			$onlyNumbers = preg_replace('/([^\d]+)/', '', $value);
		    return (strlen($onlyNumbers) == 11);
		});

        Validator::extend('cpf_cnpj', function ($attribute, $value, $parameters, $validator) {
            $onlyNumbers = preg_replace('/([^\d]+)/', '', $value);
            return (strlen($onlyNumbers) == 11 || strlen($onlyNumbers) == 14);
        });

		$messages = [
			'required' 	=> 'O campo ":attribute" é obrigatório.',
		    'max'     	=> 'O campo ":attribute" deve ter no máximo :max caracteres.',
		    'phone_br' 	=> 'O campo ":attribute" deve ser preenchido com DDD e o número com 8 ou 9 dígitos.',
		    'present' 	=> 'Você precisa confirmar o preenchimento correto do formulário.',
            'cpf_cnpj'  => 'O campo ":attribute" está inválido.',
            'loja'      => 'Selecione":attribute"',
		];

		$validator = Validator::make($inputs, $rules, $messages);
		return $validator;
	}

    private function sendEmail(&$request, &$rules, $keySetting, $subject, $layout = null)
    {
        $requestData = $request->all();
        $validator = $this->getValidator($requestData, $rules);

        if ($validator->fails()) {
        	$request->error = $validator->errors();

        } else {
            
	        $mailable = new Contato($requestData, $layout);
	        $mailable->subject($subject);

	        if(!empty($requestData['files'])){
	            foreach ($request->files as $field => $file) {
                    $mailable->attach( $requestData['files']->getRealPath(), [
                            'as' =>  $requestData['files']->getClientOriginalName(),
                            'mime' => $requestData['files']->getMimeType()
                        ] );
	            }
	            $has_file = true;
	        }

            $emails = strpos($requestData['to_address'], ',') > 0 ? explode(',', $requestData['to_address']) : $requestData['to_address'];
            // $emails = ['gabriella.vidal@novam3.com', 'gabriellavidal2013@gmail.com'];
            unset($requestData['to_address']);

	        try {
                $requestData['descomplicar'] = true;

                $mailableDescomplicar = new Contato($request, $layout);
                $mailableDescomplicar->subject($subject);

                Mail::to($emails)->send($mailable);

	        } catch(\Exception $e){
                dd($e);

	        } finally{

                // $this->send_syonet($requestData);
                $this->postLeadsDescomplicar($keySetting, $requestData, $mailableDescomplicar);
                
            }
        }
    }

    //syonet especifico pernambuco motos
    private function send_syonet($request) {
        /**
         *
         * ////////////
         * // SYONET //
         * ////////////
         *
         *
         * BEBERIBE: 7
         * BOA VIAGEM: 5
         * CAMARAGIBE: 9
         * ENCRUZILHADA: 6
         * IGARASSU: 3
         * JABOATAO: 2
         * OLINDA: 8
         * PAUDALHO: 4
         * PAULISTA: 1 # Matriz
         */
        $empresa_id   = 1;
        $chave        = '3301c9861a9026e70880e8da72258348';
        $url          = 'https://carros.novam3.com/syonet/' . $chave . '/' . $empresa_id;
        $origem       = 'INTERNET';
        $midia        = 'SITE PERNAMBUCO';
        $grupo_evento = 'OPORTUNIDADE';

        $nome        = @$request['nome'];
        $assunto     = 'CONTATO ATRAVÉS DO FORMULÁRIO: "' . ucfirst($request['form']) . '"';
        $tipo_evento = $request['form'] == 'seminovo' ? 'SEMINOVOS' : 'NOVOS';

        if($request['form'] == 'consorcio'){
            $tipo_evento = 'CONSORCIO';
        }

        $email       = @$request['email'];
        $Residencial = @$request['telefone'];

        $observacao = '<p>Contato realizado através do formulário: <strong>"' . ucfirst($request['form']) . '"</strong></p>';
        foreach($request as $chave => $valor) {
            if(is_string($valor) && !in_array($chave, ['nome', 'email', 'telefone']) && trim($valor) != '') {
                $observacao .= '<p><strong>' . ucfirst($chave) . ':</strong> ' . $valor . '</p>';
            }
        }
        dd(isset($request['carro']));
        if(isset($request['carro'])) {
            $observacao .= '<p><strong>Moto/Quadriciclo/Produto:</strong> ' . $request['carro'] . '</p>';
        } 

        if(!in_array($request['form'], ['seguro', 'contato', 'oficina'])){

            $dados =  [
                'nome'        => $nome,
                'assunto'     => $assunto,
                'grupoEvento' => $grupo_evento,
                'tipoEvento'  => $tipo_evento,
                'email'       => $email,
                'observacao'  => $observacao,
                'midia'       => $midia,
                'Residencial' => $Residencial
            ];

            // debug($dados);
            $dados = http_build_query($dados);

            $ch  = curl_init( $url );
            curl_setopt( $ch, CURLOPT_POST, 1);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $dados);
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt( $ch, CURLOPT_HEADER, 0);
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);

            $response = json_decode(curl_exec( $ch ));
            // dump($response);
            // debug($response); die();
        }
    }

    private function postLeadsDescomplicar($keySetting, $requestData, &$mailableDescomplicar)
    {
        $form = $requestData['form'];
        unset($requestData['form']);
        // $urlGetFields = 'http://api.carros.novam3.com/leads/getFields?key='.$this->carros_key.'&form='.$form;
        $url = 'http://api.carros.novam3.com/leads/insertLead?key=' . \App\Models\AppDescomplicarModel::$COMPANY_KEY . '&form=' . $form;
        // $url = 'http://localhost/new-carros-api-2015/leads/insertLead?key=' . \App\Models\AppDescomplicarModel::$COMPANY_KEY . '&form=' . $form;
        dump($url);
        if (isset($requestData['nome'])) {
            $dados = [
                'nome' => @$requestData['nome'],
                'email' => @$requestData['email'],
                'telefone' => @$requestData['telefone'],
                'unidade' => @$requestData['unidade']
            ];
            unset($requestData['nome']);
            unset($requestData['email']);
            unset($requestData['telefone']);
            unset($requestData['unidade']);
        }

        if(in_array($form, ['oferta', 'consorcio', 'nova']) === true){
            $dados['vehicle_name'] = @$requestData['vehicle_name'];
            $dados['vehicle_slug'] = @$requestData['vehicle_slug'];
            $dados['vehicle_url'] = @$requestData['vehicle_url'];

            unset($requestData['vehicle_name']);
            unset($requestData['vehicle_slug']);
            unset($requestData['vehicle_url']);
        }

        $view = \Illuminate\Support\Facades\View::make($mailableDescomplicar->layout, ['requestData' => $requestData, 'descomplicar' => $requestData['descomplicar']]);
        $dados['mensagem'] = $view->render();
        // dump($form);
        // dump($dados);
        $dados = http_build_query($dados);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dados);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        // dump($response);
        // dump(curl_getinfo($ch, CURLINFO_HTTP_CODE));
        // echo html_entity_decode($response); die();
    }

	public function contato(Request $request)
    {
    	$rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br',
            'assunto' => 'required',
            'mensagem' => 'required',
            'unidade' => 'required'
        ];

        $this->sendEmail($request, $rules, 'email_contato', 'Contato através do formulário de Contato');

    	if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function seguro(Request $request)
    {
        $rules = [
            'nome' => 'required|max:255',
            // 'email' => 'required|email',
            'telefone' => 'required|phone_br',
            'cidade' => 'required',
            'unidade' => 'required',
            'fabricante' => 'required',
            'modelo' => 'required',
            'placa' => 'required',
            'ano' => 'required',
            'finalidade_1' => 'required',
            'finalidade_2' => 'required',
            'finalidade_3' => 'required',
            'faixa_etaria' => 'required',
            'data_nascimento' => 'required',
            'genero' => 'required',
            'idade_cnh' => 'required',
        ];

        $this->sendEmail($request, $rules, 'email_contato', 'Contato através do formulário de Seguros');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    private function getPropostaRulesCarro()
    {
        return [
            'carro' => 'required',
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br',
            'unidade' => 'required',
            'pagamento' => 'required'
        ];
    }

    public function propostaOferta(Request $request)
    {
        $rules = $this->getPropostaRulesCarro();

        if (!empty($request->carro)) {
            $request->oferta = Models\Showroom::customOfertaBySlug($request->carro);
        }

        $this->sendEmail($request, $rules, 'email_oferta', 'Contato através do formulário de Oferta');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function propostaNovo(Request $request)
    {
        $rules = $this->getPropostaRulesCarro();

        if (!empty($request->carro)) {
            $request->modelo = Models\Showroom::customNovoBySlug($request->modelo);
        }

        $this->sendEmail($request, $rules, 'email_novo', 'Contato através do formulário de Novo');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function propostaSeminovo(Request $request)
    {

        $rules = $this->getPropostaRulesCarro();

        $this->sendEmail($request, $rules, 'email_seminovo', 'Contato através do formulário de Seminovo');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function propostaConsorcio(Request $request)
    {
        $rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br'
        ];

        $this->sendEmail($request, $rules, 'email_consorcio', 'Contato através do formulário de Consórcio');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function realizarAgendamento(Request $request)
    {
        // dd($request);
        $rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required',
            'hora1' => 'required',
            'data1' => 'required',
            'hora2' => 'required',
            'data2' => 'required',
            'km' => 'required',
            'modelo' => 'required',
            'ano' => 'required',
            'placa' => 'required',
            'mensagem' => 'required',
        ];

        $this->sendEmail($request, $rules, 'email_agendamento', 'Contato através do formulário de Agendamento de Revisão');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    public function trabalheConosco(Request $request)
    {
        $rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br',
            'mensagem' => 'required',
            'unidade' => 'required'
        ];

        $this->sendEmail($request, $rules, 'email_trabalhe_conosco', 'Contato através do formulário de Trabalhe Conosco');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = true;
        }
        return response()->json($response);
    }

    //Alagoas Motos
    public function propostaAcessorios(Request $request)
    {
          $rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br',
            'unidade' => 'required',
            'pagamento' => 'required'
        ];

        $this->sendEmail($request, $rules, 'email_pecas', 'Contato através do formulário de Acessorios');

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = $request->all();
        }
        return response()->json($response);
    }

    public function Autovemacontato(Request $request)
    {
        // dd($request->all());
       
        if($request->idOferta != "null"){
            $oferta = $this->getOfertaId($request->idOferta);

            switch($oferta->payment_method){
                case "bonus":
                    $value = "Bônus de R$ ". number_format($oferta->bonus,2,',','.');
                break;
                
                case "parcel_no_entry":
                    $value = $oferta->parcel_amount."x de R$ ".number_format($oferta->parcel_value,2,',','.');
                break;
            
                case "parcel_w_entry":
                    $value = "Entrada de R$ ".number_format($oferta->entry,2,',','.')." + ".$oferta->parcel_amount."X de ".number_format($oferta->parcel_value,2,',','.');
                break;

                case "desconto":
                    $value = "De R$ ".number_format($oferta->value_to,2,',','.')." por R$ ".number_format($oferta->value_to,2,',','.');
                break;

                default:
            }

            $request['value'] = $value;
            
        }

        if($request->idSeminovo != "null"){
            $seminovo = Models\Showroom::customSeminovoBySlugId($request->idSeminovo);
            $request['value'] = "Por apenas R$ ".number_format($seminovo->used_value,2,',','.');    
        }

        // dd($request->all());

        if($request->idDirect !== "null"){
            $direct = Models\Showroom::customVendaDiretaById($request->idDirect);
            $request['value'] = "Por apenas R$ ".number_format(@$direct->value_from,2,',','.');    
        }


        $request['carro'] = isset($oferta->slug) ? $oferta->slug : $request->mensagem;

        //  dd($request->all());

        $rules = [
            'nome' => 'required|max:255',
            'email' => 'required|email',
            'telefone' => 'required|phone_br',
        ];

        //send lead
        $this->send_followize($request, $request->form);

        $this->sendEmail($request, $rules, 'email_contato', 'Contato através do formulário de '. $request->form);

        if($request->error){
            $response['error'] = $request->error;
        } else{
            $response['success'] = $request->all();
        }
        return response()->json($response);
    }

    public function getOfertaId($id)
    {
        $oferta = Models\Showroom::customOfertaById($id);
        return $oferta;
    }

    public function send_followize($request,$keySettingSuffix)
    {
        /*
        *Integração com o Followize
        *@Autor: Felipe Mendonça
        */

        $product = [];
        if(isset($request['carro']))
            $product[] = str_replace('-', ' ', $request['carro']);

        if(isset($request['consorcio']))
            $product[] = str_replace('-', ' ', $request['consorcio']);

        if(isset($request['catalog']))
            $product[] = str_replace('-', ' ', $request['catalog']);

        if(isset($request['parcelas']))
            $obs = "Financiamento - Entrada: R$".$request['entrada']." Parcelas: ".$request['parcelas'];

        // if($keySettingSuffix == 'novos' || $keySettingSuffix == 'ofertas' || $keySettingSuffix == 'seminovos' || $keySettingSuffix == 'vendas_diretas' || $keySettingSuffix == 'consorcios'){
            $key = '741de2bd6943605dad36fcda22eafe89';
        // }else{
        //     $key = '72ae64ba6f72951cb335720e01546441';
        // }
        
        $apiURL = "https://www.followize.com.br/api/v2/Leads/";

        $data["clientKey"] = 'f8ee27f715ec74194d0b093feeb82a1b';
        $data["teamKey"] = $key;
        $data["conversionGoal"] = "Site Autovema Motors";
        $data["name"] = $request['nome'];
        isset($request['email']) ? $data["email"] = $request['email'] : $data["email"] = 'email@obrigatorio.com';
        isset($request['telefone']) ? $data["phone"] = $request['telefone'] : '';
        isset($request['mensagem']) ? $data["message"] = $request['mensagem'] : '';
        $data["hubUtmz"] = "Site Autovema Motors";
        !empty($product) ? $data['productTitle'] = $product[0] : '';
        isset($obs) ? $data['productRefer'] = $obs : '';
        $data['categoryTitle'] = $keySettingSuffix;
        isset($request['value']) ? $data['categoryRefer'] = "Valor da oferta: ".$request['value'] : '';
        $data['locationTitle'] = "Site Autovema Motors";
        $data['brandTitle'] = "Mitisubshi";

        if($keySettingSuffix == 'analise'){
            $data['message']  = isset($request['nascimento']) ? "Dados do cliente: Nascimento: ".$request['nascimento'] ." / " : '' . 
            $data['message'] .= isset($request['naturalidade']) ? "Naturalidade: ".$request['naturalidade'] ." / ": '' ;
            $data['message'] .= isset($request['nacionalidade']) ? "Nacionalidade: ".$request['nacionalidade']." / " : '';
            $data['message'] .= isset($request['sexo']) ? "Sexo: ".$request['sexo']." / " : '';
            $data['message'] .= isset($request['cpf']) ? "CPF: ".$request['cpf'] ." / ": '';
            $data['message'] .= isset($request['doc_identidade']) ? "Identidade: ".$request['doc_identidade'] ." / ":  '';
            $data['message'] .= isset($request['org_emissor']) ? "Orgão emissor: ".$request['org_emissor']." / " : '';
            $data['message'] .= isset($request['nome_mae']) ? "Nome da Mãe: ".$request['nome_mae']." / " : '';
            $data['message'] .= isset($request['nome_pai']) ? "Nome do Pai: ".$request['nome_pai']." / " : '';
            $data['message'] .= isset($request['estado_civil']) ? "Estado civil: ".$request['estado_civil']." / " : '';
            $data['message'] .= isset($request['tipo_residencia']) ? "Tipo de Residencia: ".$request['tipo_residencia']." / " : '';
            $data['message'] .= isset($request['tempo_residencia']) ? "Tempo Residencia: ".$request['tempo_residencia']." / " : '';
            $data['message'] .= isset($request['end_residencia']) ? "Endereço Residencial: ".$request['end_residencia']." / " : '';
            $data['message'] .= isset($request['numero_residencia']) ? "Número Residencia: ".$request['numero_residencia']." / " : '';
            $data['message'] .= isset($request['bairro_residencia']) ? "Bairro Residencia: ".$request['bairro_residencia']." / " : '';
            $data['message'] .= isset($request['complemento_residencia']) ? "Complemento Residencia: ".$request['complemento_residencia']." / " : '';
            $data['message'] .= isset($request['cep_residencia']) ? "Cep residencia: ".$request['cep_residencia']." / " : '';
            $data['message'] .= isset($request['cidade_residencia']) ? "Cidade residencia: ".$request['cidade_residencia']." / " : '';
            $data['message'] .= isset($request['uf_residencia']) ? "Estado: ".$request['uf_residencia']." / " : '';
            $data['message'] .= isset($request['tel_residencia']) ? "Telefone residencia: ".$request['tel_residencia']." / " : '';
            $data['message'] .= isset($request['tel_celular']) ? "Celular residencia: ".$request['tel_celular']." / " : '';
            $data['message'] .= isset($request['razao_social_comercial']) ? "Fonte de renda: Razão social: ".$request['razao_social_comercial']." / " : '';
            $data['message'] .= isset($request['cnpj_comercial']) ? "CNPJ: ".$request['cnpj_comercial']." / " : '';
            $data['message'] .= isset($request['ocupacao_comercial']) ?"Ocupação/Função: ".$request['ocupacao_comercial']." / " : '';
            $data['message'] .= isset($request['admissao_comercial']) ? "Admissão: ".$request['admissao_comercial']." / " : '';
            $data['message'] .= isset($request['salario_comercial']) ? "Salário: ".$request['salario_comercial']." / " : '';
            $data['message'] .= isset($request['end_comercial']) ? "Endereço: ".$request['end_comercial']." / " : '';
            $data['message'] .= isset($request['numero_comercial']) ? "Número: ".$request['numero_comercial']." / " : '';
            $data['message'] .= isset($request['bairro_comercial']) ? "Bairro: ".$request['bairro_comercial']." / " : '';
            $data['message'] .= isset($request['complemento_comercial']) ? "Complemento: ".$request['complemento_comercial']." / " : '';
            $data['message'] .= isset($request['cidade_comercial']) ? "Cidade: ".$request['cidade_comercial']." / " : '';
            $data['message'] .= isset($request['uf_comercial']) ? "Estado: ".$request['uf_comercial']." / " : '';
            $data['message'] .= isset($request['tel_comercial']) ? "Telefone: ".$request['tel_comercial']." / " : '';
            $data['message'] .= isset($request['razao_social_comercial2']) ? "Outras fontes de renda: Razão social: ".$request['razao_social_comercial2']." / " : '';
            $data['message'] .= isset($request['cnpj_comercial2']) ? "CNPJ: ".$request['cnpj_comercial2']." / " : '';
            $data['message'] .= isset($request['ocupacao_comercial2']) ?"Ocupação/Função: ".$request['ocupacao_comercial2']." / " : '';
            $data['message'] .= isset($request['admissao_comercial2']) ? "Admissão: ".$request['admissao_comercial2']." / " : '';
            $data['message'] .= isset($request['salario_comercial2']) ? "Salário: ".$request['salario_comercial2']." / " : '';
            $data['message'] .= isset($request['end_comercial2']) ? "Endereço: ".$request['end_comercial2']." / " : '';
            $data['message'] .= isset($request['numero_comercial2']) ? "Número: ".$request['numero_comercial2']." / " : '';
            $data['message'] .= isset($request['bairro_comercial2']) ? "Bairro: ".$request['bairro_comercial2']." / " : '';
            $data['message'] .= isset($request['complemento_comercial2']) ? "Complemento: ".$request['complemento_comercial2']." / " : '';
            $data['message'] .= isset($request['cidade_comercial2']) ? "Cidade: ".$request['cidade_comercial2']." / " : '';
            $data['message'] .= isset($request['uf_comercial2']) ? "Estado: ".$request['uf_comercial2']." / " : '';
            $data['message'] .= isset($request['tel_comercial2']) ? "Telefone: ".$request['tel_comercial2']." / " : '';
            $data['message'] .= isset($request['nome_conjuge']) ? "Cônjuge/Avalista: Nome: ".$request['nome_conjuge']. " / " : '' ;
            $data['message'] .= isset($request['nascimento_conjuge']) ? "Nascimento: ".$request['nascimento_conjuge'] ." / " : '' ;
            $data['message'] .= isset($request['naturalidade_conjuge']) ? "Naturalidade: ".$request['naturalidade_conjuge'] ." / ": '';
            $data['message'] .= isset($request['nacionalidade_conjuge']) ? "Nacionalidade: ".$request['nacionalidade_conjuge']." / " : '';
            $data['message'] .= isset($request['sexo_conjuge']) ? "Sexo: ".$request['sexo_conjuge']." / " : '';
            $data['message'] .= isset($request['cpf_conjuge']) ? "CPF: ".$request['cpf_conjuge'] ." / ":  '';
            $data['message'] .= isset($request['doc_identidade_conjuge']) ? "Identidade: ".$request['doc_identidade_conjuge'] ." / ":  '';
            $data['message'] .= isset($request['org_emissor_conjuge']) ? "Orgão emissor: ".$request['org_emissor_conjuge']." / " : '';
            $data['message'] .= isset($request['nome_mae_conjuge']) ? "Nome da Mãe: ".$request['nome_mae_conjuge']." / " : '';
            $data['message'] .= isset($request['nome_pai_conjuge']) ? "Nome do Pai: ".$request['nome_pai_conjuge']." / " : '';
            $data['message'] .= isset($request['empresa_trabalha_conjuge']) ? "Empresa onde trabalha: ".$request['empresa_trabalha_conjuge']." / " : '';
            $data['message'] .= isset($request['tempo_servico_conjuge']) ? "Tempo de serviço: ".$request['tempo_servico_conjuge']." / " : '';
            $data['message'] .= isset($request['cargo_conjuge']) ? "Cargo função: ".$request['cargo_conjuge']." / " : '';
            $data['message'] .= isset($request['salario_conjuge']) ? "Salário: ".$request['salario_conjuge']." / " : '';
            $data['message'] .= isset($request['end_comercial_conjuge']) ? "Endereço: ".$request['end_comercial_conjuge']." / " : '';
            $data['message'] .= isset($request['cep_comercial_conjuge']) ? "Cep: ".$request['cep_comercial_conjuge']." / " : '';
            $data['message'] .= isset($request['cidade_comercial_conjuge']) ? "Cidade : ".$request['cidade_comercial_conjuge']." / " : '';
            $data['message'] .= isset($request['uf_comercial_conjuge']) ? "Estado: ".$request['uf_comercial_conjuge']." / " : '';
            $data['message'] .= isset($request['banco']) ? "Referência Bancária: Banco: ".$request['banco']." / " : '';
            $data['message'] .= isset($request['n_banco']) ? "Telefone: ".$request['n_banco']." / " : '';
            $data['message'] .= isset($request['agencia']) ? "Agencia: ".$request['agencia']." / " : '';
            $data['message'] .= isset($request['n_conta']) ? "Conta: ".$request['n_conta']." / " : '';
            $data['message'] .= isset($request['tipo_conta']) ? "Tipo: ".$request['tipo_conta']." / " : '';
            $data['message'] .= isset($request['cl_desde']) ? "CL desde: ".$request['cl_desde']." / " : '';
            $data['message'] .= isset($request['nome_referencia']) ? "Referência pessoal/comercial: Nome: ".$request['nome_referencia']." / " : '';
            $data['message'] .= isset($request['afinidade_referencia']) ? "Afinidade: ".$request['afinidade_referencia']." / " : '';
            $data['message'] .= isset($request['tel_residencia_referencia']) ? "Fixo: ".$request['tel_residencia_referencia']." / " : '';
            $data['message'] .= isset($request['tel_celular_referencia']) ? "Celular: ".$request['tel_celular_referencia']." / " : '';
            $data['message'] .= isset($request['analise_comentario']) ? "Observações: ".$request['analise_comentario'] : '';
        }

        $data = json_encode($data);
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $apiURL);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        $result = json_decode(curl_exec($curl), true);
        // dd($result);
        curl_close($curl);

    }

    //end autovema

}
